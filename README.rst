timetools
---------

|pipeline| |coverage|

.. |pipeline| image:: https://gitlab.com/blueskyjunkie/timetools/badges/master/pipeline.svg
   :target: https://gitlab.com/blueskyjunkie/timetools/commits/master
   :alt: pipeline status

.. |coverage| image:: https://gitlab.com/blueskyjunkie/timetools/badges/master/coverage.svg
   :target: https://gitlab.com/blueskyjunkie/timetools/commits/master
   :alt: coverage report

|pypiVersion|

|doi0.6.0| (v0.6.0)

.. |pypiVersion| image:: https://badge.fury.io/py/timetools.svg
   :target: https://badge.fury.io/py/timetools
   :alt: PyPI version


Python utilities relating to synchronization analysis and visualization such as time error, MTIE,
TDEV and others. PDV generation using methods related to  ITU-T G.8263 are also included.


.. contents::

.. section-numbering::


Getting started
===============

The simplest way to acquire ``timetools`` is using ``pip``.

.. code-block:: bash

   pip install timetools


Dependencies
^^^^^^^^^^^^

- Python >=3.6
- numpy/scipy
- matplotlib

For Windows, it is highly recommended to use the
`Anaconda distribution of Python <https://www.anaconda.com/distribution/#download-section>`_
because to date all the necessary dependencies are included in Anaconda; no additional installation
necessary.


License
=======

GPL v3 (or newer, your choice). See `LICENSE <LICENSE>`_ for the details.


DOI Archive
===========

+-------+------------+
| 0.4.0 | |doi0.4.0| |
+-------+------------+
| 0.5.0 | |doi0.5.0| |
+-------+------------+
| 0.6.0 | |doi0.6.0| |
+-------+------------+

.. |doi0.4.0| image:: https://zenodo.org/badge/DOI/10.5281/zenodo.15797.svg
   :target: https://doi.org/10.5281/zenodo.15797
   :alt: DOI 0.4.0

.. |doi0.5.0| image:: https://zenodo.org/badge/DOI/10.5281/zenodo.15994.svg
   :target: https://doi.org/10.5281/zenodo.15994
   :alt: DOI 0.5.0

.. |doi0.6.0| image:: https://zenodo.org/badge/DOI/10.5281/zenodo.268727.svg
   :target: https://doi.org/10.5281/zenodo.268727
   :alt: DOI 0.6.0
