#
# Copyright 2017 Russell Smiley
#
# This file is part of timetools.
#
# timetools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# timetools is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with timetools.  If not, see <http://www.gnu.org/licenses/>.
#

import unittest

import matplotlib.pyplot as mpp

import timetools.synchronization.clock as sc
import timetools.synchronization.compliance.itut_g8263.compute as tscg8263
import timetools.synchronization.compliance.itut_g8263.holdoverTransient as tscg8263h
import timetools.synchronization.compliance.itut_g8263.wanderGeneration as tscg8263wg
import timetools.synchronization.compliance.visualization as tscv
import timetools.synchronization.oscillator as tso
import timetools.synchronization.oscillator.noise.gaussian as tsong
import timetools.synchronization.time as st


class TestItuTG8263(unittest.TestCase):
    def testConstantTemperatureWanderGenerationMask(self):
        this_mask = tscg8263wg.constant_temperature_mtie_ns_wander_generation_mask

        figure_handle = mpp.figure()
        # Set the plot limits before the mask plot so that it will figure out
        # appropriate ranges in the absence of signal data
        mpp.xlim((0.01, 20e3))
        mpp.ylim((100, 200e3))
        this_mask.add_to_plot(figure_handle.number)

        mpp.yscale("log")
        mpp.xscale("log")
        mpp.grid(which="minor")
        mpp.title(self.testConstantTemperatureWanderGenerationMask.__name__)

    def testVariableTemperatureWanderGenerationMask(self):
        constant_temperature_mask = (
            tscg8263wg.constant_temperature_mtie_ns_wander_generation_mask
        )
        this_mask = tscg8263wg.variable_temperature_mtie_ns_wander_generation_mask

        figure_handle = mpp.figure()
        # Set the plot limits before the mask plot so that it will figure out
        # appropriate ranges in the absence of signal data
        mpp.xlim((0.01, 20e3))
        mpp.ylim((100, 200e3))
        this_mask.add_to_plot(figure_handle.number, color="b")
        constant_temperature_mask.add_to_plot(
            figure_handle.number, color="r", linestyle="--"
        )

        mpp.yscale("log")
        mpp.xscale("log")
        mpp.grid(which="minor")
        mpp.title(self.testVariableTemperatureWanderGenerationMask.__name__)

    def testHoldoverTransientPhaseErrorMask(self):
        this_mask = tscg8263h.phaseErrorNs

        figure_handle = mpp.figure()
        mpp.title(self.testHoldoverTransientPhaseErrorMask.__name__)
        # Set the plot limits before the mask plot so that it will figure out
        # appropriate ranges in the absence of signal data
        mpp.xlim((0, 100))
        mpp.ylim((-1000, 1000))
        mpp.grid()
        this_mask.add_to_plot(figure_handle.number, linewidth=4, color="b", marker="o")

        mpp.grid(which="minor")

    def testHoldoverTransientFfoMask(self):
        this_mask = tscg8263h.ffoPpb

        figure_handle = mpp.figure()
        mpp.title(self.testHoldoverTransientFfoMask.__name__)
        # Set the plot limits before the mask plot so that it will figure out
        # appropriate ranges in the absence of signal data
        mpp.xlim((0, 24 * 3600))
        mpp.ylim((-15, 15))
        mpp.grid()
        this_mask.add_to_plot(figure_handle.number, linewidth=4, color="b", marker="o")

        mpp.grid(which="minor")

    def testHoldoverTransientFfoRateMask(self):
        this_mask = tscg8263h.ffoRatePpbPerSecond

        figure_handle = mpp.figure()
        mpp.title(self.testHoldoverTransientFfoRateMask.__name__)
        # Set the plot limits before the mask plot so that it will figure out
        # appropriate ranges in the absence of signal data
        mpp.xlim((0, 3600))
        mpp.ylim((-2e-5, 2e-5))
        mpp.grid()
        this_mask.add_to_plot(figure_handle.number, linewidth=4, color="b", marker="o")

        mpp.grid(which="minor")

    def testWanderGenerationConstantTemperatureNs1(self):
        time_step_seconds = 1
        number_samples = 10000

        desired_number_observations = 15

        clock_ffo_ppb = 0.5
        clock_rms_jitter_ppb = 2

        reference_time_generator = st.ReferenceGenerator(time_step_seconds)
        reference_time_seconds = reference_time_generator.generate(number_samples)

        clock_model = sc.ClockModel(
            tso.OscillatorModel(
                initial_ffo_ppb=clock_ffo_ppb,
                noiseModel=tsong.GaussianNoise(
                    standard_deviation_ppb=clock_rms_jitter_ppb, seed=1459
                ),
            )
        )
        local_time_seconds, instantaneous_lo_ffo_ppb = clock_model.generate(
            reference_time_seconds
        )

        analysis_result, this_mask, mtie_data = tscg8263.analyze_itut_g8263_mask(
            local_time_seconds,
            reference_time_seconds,
            time_step_seconds,
            desired_number_observations,
        )

        this_plot = tscv.plot()

        this_plot.addMask(this_mask, linewidth=4, color="r", linestyle="--", marker="o")
        this_plot.addSignal(mtie_data)

        this_plot.go()

        mpp.yscale("log")
        mpp.xscale("log")
        mpp.grid(which="minor")
        mpp.title(self.testWanderGenerationConstantTemperatureNs1.__name__)

        self.assertTrue(analysis_result, "Failed 16 ppb mask when should not have")

    def testWanderGenerationConstantTemperatureNs2(self):
        time_step_seconds = 1
        number_samples = 10000

        desired_number_observations = 15

        clock_ffo_ppb = 5
        clock_rms_jitter_ppb = 2

        reference_time_generator = st.ReferenceGenerator(time_step_seconds)
        reference_time_seconds = reference_time_generator.generate(number_samples)

        clock_model = sc.ClockModel(
            tso.OscillatorModel(
                initial_ffo_ppb=clock_ffo_ppb,
                noiseModel=tsong.GaussianNoise(
                    standard_deviation_ppb=clock_rms_jitter_ppb, seed=1459
                ),
            )
        )
        local_time_seconds, instantaneous_lo_ffo_ppb = clock_model.generate(
            reference_time_seconds
        )

        analysis_result, this_mask, mtie_data = tscg8263.analyze_itut_g8263_mask(
            local_time_seconds,
            reference_time_seconds,
            time_step_seconds,
            desired_number_observations,
        )

        this_plot = tscv.plot()

        this_plot.addMask(this_mask, linewidth=4, color="r", linestyle="--")
        this_plot.addSignal(mtie_data, linestyle="--", marker="o")

        this_plot.go()

        mpp.yscale("log")
        mpp.xscale("log")
        mpp.grid(which="minor")
        mpp.title(self.testWanderGenerationConstantTemperatureNs2.__name__)

        self.assertFalse(analysis_result, "Passed 16 ppb mask when should not have")

    def tearDown(self):
        if __name__ == "__main__":
            mpp.show()


if __name__ == "__main__":
    unittest.main()
