#
# Copyright 2017 Russell Smiley
#
# This file is part of timetools.
#
# timetools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# timetools is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with timetools.  If not, see <http://www.gnu.org/licenses/>.
#

import matplotlib.pyplot as mpp

import timetools.synchronization.compliance.itut_g8261.deployment_case1.wanderBudget as tscg8261dc1wb
import timetools.synchronization.compliance.itut_g8261.deployment_case2.wanderBudget as tscg8261dc2awb
import timetools.synchronization.compliance.itut_g8261.eec_option1.networkWander as tscg8261eec1nw
import timetools.synchronization.compliance.itut_g8261.eec_option2.networkWander as tscg8261eec2nw


class TestItuTG8261:
    def test_eec_option1_mtie_mask(self):
        this_mask = tscg8261eec1nw.mtieNs

        figure_handle = mpp.figure()
        mpp.title(self.test_eec_option1_mtie_mask.__name__)
        # Set the plot limits before the mask plot so that it will figure out
        # appropriate ranges in the absence of signal data
        mpp.xlim((0.1, 100000))
        mpp.ylim((100, 10000))
        this_mask.add_to_plot(figure_handle.number)

        mpp.yscale("log")
        mpp.xscale("log")
        mpp.grid(which="minor")

    def test_eec_option1_tdev_mask(self):
        this_mask = tscg8261eec1nw.tdevNs

        figure_handle = mpp.figure()
        mpp.title(self.test_eec_option1_tdev_mask.__name__)
        # Set the plot limits before the mask plot so that it will figure out
        # appropriate ranges in the absence of signal data
        mpp.xlim((0.1, 100000))
        mpp.ylim((10, 1000))
        this_mask.add_to_plot(figure_handle.number)

        mpp.yscale("log")
        mpp.xscale("log")
        mpp.grid(which="minor")

    def test_eec_option2_tdev_mask(self):
        this_mask = tscg8261eec2nw.tdevNs

        figure_handle = mpp.figure()
        mpp.title(self.test_eec_option2_tdev_mask.__name__)
        # Set the plot limits before the mask plot so that it will figure out
        # appropriate ranges in the absence of signal data
        mpp.xlim((0.01, 1000))
        mpp.ylim((1, 1000))
        this_mask.add_to_plot(figure_handle.number)

        mpp.yscale("log")
        mpp.xscale("log")
        mpp.grid(which="minor")

    def test_dc2_a_mrtie_mask(self):
        this_mask = tscg8261dc2awb.case2A2048MrtieMicroseconds

        figure_handle = mpp.figure()
        mpp.title(self.test_dc2_a_mrtie_mask.__name__)
        # Set the plot limits before the mask plot so that it will figure out
        # appropriate ranges in the absence of signal data
        mpp.xlim((0.01, 1000))
        mpp.ylim((1, 100))
        this_mask.add_to_plot(figure_handle.number)

        mpp.yscale("log")
        mpp.xscale("log")
        mpp.grid(which="minor")

    def test_dc11544_mrtie_mask(self):
        this_mask = tscg8261dc1wb.case11544MrtieMicroseconds

        figure_handle = mpp.figure()
        mpp.title(self.test_dc11544_mrtie_mask.__name__)
        # Set the plot limits before the mask plot so that it will figure out
        # appropriate ranges in the absence of signal data
        mpp.xlim((0.01, 100000))
        mpp.ylim((0.01, 10))
        this_mask.add_to_plot(figure_handle.number)

        mpp.yscale("log")
        mpp.xscale("log")
        mpp.grid(which="minor")

    def test_dc12048_mrtie_mask(self):
        this_mask = tscg8261dc1wb.case12048MrtieMicroseconds

        figure_handle = mpp.figure()
        mpp.title(self.test_dc12048_mrtie_mask.__name__)
        # Set the plot limits before the mask plot so that it will figure out
        # appropriate ranges in the absence of signal data
        mpp.xlim((0.01, 1000))
        mpp.ylim((0.01, 10))
        this_mask.add_to_plot(figure_handle.number)

        mpp.yscale("log")
        mpp.xscale("log")
        mpp.grid(which="minor")
