#
# Copyright 2017 Russell Smiley
#
# This file is part of timetools.
#
# timetools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# timetools is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with timetools.  If not, see <http://www.gnu.org/licenses/>.
#

"""Python utilities relating to synchronization analysis and visualization such as time error, MTIE,
TDEV and others. PDV generation using methods related to  ITU-T G.8263 are also included.
"""

import os.path

HERE = os.path.abspath(os.path.dirname(__file__))
VERSION_FILE_PATH = os.path.abspath(os.path.join(HERE, "VERSION"))

with open(VERSION_FILE_PATH) as version_file:
    THIS_VERSION = version_file.read().strip()

__version__ = THIS_VERSION
