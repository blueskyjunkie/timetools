#
# Copyright 2017 Russell Smiley
#
# This file is part of timetools.
#
# timetools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# timetools is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with timetools.  If not, see <http://www.gnu.org/licenses/>.
#

import math
import typing

import matplotlib.pyplot as mpp
import numpy


def decimate_pdv_to_plot_resolution(
    x_data: numpy.array, y_data: numpy.array, plot_resolution: int, plot_width: int
) -> typing.Tuple[numpy.array, numpy.array]:
    assert len(x_data) == len(y_data)

    original_number_samples = len(y_data)

    data_resolution = original_number_samples / plot_width
    decimation_factor = math.floor(data_resolution / plot_resolution)

    if decimation_factor != 0:
        # In the case of PDV we are mostly interested in the minima and maxima
        # so preserve those features in decimation block.
        number_windows = math.floor(original_number_samples / decimation_factor)
        number_features = 2 * number_windows

        input_data = (x_data, y_data)
        decimated_data = (numpy.zeros(number_features), numpy.zeros(number_features))

        for k in numpy.arange(0, number_windows):
            for m in range(0, 2):
                window_offset = decimation_factor * k
                window_indices = numpy.arange(
                    window_offset, (window_offset + decimation_factor)
                )

                decimated_data[m][2 * k] = numpy.min(input_data[m][window_indices])
                decimated_data[m][(2 * k) + 1] = numpy.max(
                    input_data[m][window_indices]
                )

        return_value = decimated_data
    else:
        # The data resolution is lower than the plot resolution so decimation is unnecessary
        return_value = (x_data, y_data)

    return return_value


def plot_minimum_threshold_on_pdv(
    reference_time_seconds: numpy.array,
    input_pdv: numpy.array,
    time_step_seconds: float,
    window_duration_seconds: float,
    y_unit: str = "",
    figure_instance: typing.Optional[typing.Any] = None,
):
    plot_resolution_dpi = 300
    plot_width_inches = 8

    if figure_instance != None:
        figure_handle = mpp.figure(figure_instance)
        figure_number = figure_instance
    else:
        figure_handle = mpp.figure(
            figsize=(plot_width_inches, 5), dpi=plot_resolution_dpi
        )
        figure_number = figure_handle.number

    window_size = math.floor(window_duration_seconds / time_step_seconds)
    threshold_delay, offset_values = calculateMinimumThresholdPacketDelay(
        input_pdv, window_size
    )

    decimated_reference_time, decimated_pdv = decimate_pdv_to_plot_resolution(
        reference_time_seconds, input_pdv, plot_resolution_dpi, plot_width_inches
    )
    decimated_offset_time, decimated_threshold_delay = decimate_pdv_to_plot_resolution(
        reference_time_seconds[offset_values + window_size - 1],
        threshold_delay,
        plot_resolution_dpi,
        plot_width_inches,
    )

    mpp.plot(decimated_reference_time, decimated_pdv)
    mpp.plot(decimated_offset_time, decimated_threshold_delay)

    x_label = "Reference Time (sec)"
    mpp.xlabel(x_label)

    y_label = "Delay (" + y_unit + ")"
    mpp.ylabel(y_label)

    ax = figure_handle.gca()
    ax.grid()

    return figure_number


def plot_pdv_histogram(
    input_data: numpy.array,
    bins: int = 100,
    x_unit: str = "",
    y_unit: str = "",
    figure_instance: typing.Optional[typing.Any] = None,
):
    if isinstance(input_data, tuple):
        frequency_data, bin_data = input_data
    elif isinstance(input_data, numpy.ndarray):
        frequency_data, bin_data = numpy.histogram(input_data, bins=bins)
    else:
        raise VisualizationException("Incorrect input data")

    if figure_instance != None:
        mpp.figure(figure_instance)
        figure_number = figure_instance
    else:
        figure_handle = mpp.figure()
        figure_number = figure_handle.number

    mpp.bar(bin_data[:-1], frequency_data, width=numpy.diff(bin_data))
    mpp.xlim(min(bin_data), max(bin_data))

    x_label = "Delay bin " + x_unit
    mpp.xlabel(x_label)

    y_label = "Bin frequency " + y_unit
    mpp.ylabel(y_label)

    return figure_number
