#
# Copyright 2017 Russell Smiley
#
# This file is part of timetools.
#
# timetools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# timetools is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with timetools.  If not, see <http://www.gnu.org/licenses/>.
#

import math
import typing

import numpy
import numpy.random as nr


class Parameters:
    def __init__(
        self,
        r: float = 2.5,
        m: float = 8,
        load_cycle_duration_seconds: float = 240,
        random_seed: typing.Optional[int] = None,
    ):
        self.r = r
        self.m = m
        self.load_cycle_duration_seconds = load_cycle_duration_seconds

        self.random_generator = None
        if random_seed is not None:
            # Assume randomSeed is a 32 bit integer
            self.random_generator = nr.RandomState(random_seed)
        else:
            self.random_generator = nr.RandomState()


class Generator:
    def __init__(self, parameters):
        self._r = parameters.r
        self._m = parameters.m
        self._load_cycle_duration_seconds = parameters.load_cycle_duration_seconds
        self._random_generator = parameters.random_generator

        self._phi, self._theta = compute_flicker_coefficients(self._r, self._m)

    def generate(self, timebase_seconds: numpy.array) -> numpy.array:
        # Assume the timebase is uniformly sampled.
        # Assume that the r, m selection is constant for the duration of instantiation.

        # timebase must be monotonic
        assert numpy.all(numpy.diff(timebase_seconds) > 0)

        number_load_noise_samples = self.compute_noise_sequence_length(
            min(timebase_seconds), max(timebase_seconds)
        )

        # Rec. ITU-T G.8263/Y.1363 (2012)/Amd.2 (05/2014), pp5-10
        pn = self._random_generator.normal(size=number_load_noise_samples)
        load_flicker_noise_percent = compute_load_flicker_noise_percent(
            pn, self._phi, self._theta
        )
        self._alpha, self._beta, self._rho = compute_gamma_coefficients(
            load_flicker_noise_percent
        )

        start_times_seconds, stop_times_seconds = self.calculate_segment_bounds(
            timebase_seconds
        )

        assert len(start_times_seconds) == len(stop_times_seconds)
        assert len(start_times_seconds) == len(self._alpha)

        pdv_seconds = numpy.zeros(timebase_seconds.shape)
        for start_time_seconds, stop_time_seconds, alpha, beta, rho in zip(
            start_times_seconds, stop_times_seconds, self._alpha, self._beta, self._rho
        ):

            this_cycle_logical_indices = numpy.logical_and(
                timebase_seconds >= start_time_seconds,
                timebase_seconds <= stop_time_seconds,
            )

            this_cycle_timebase_seconds = timebase_seconds[this_cycle_logical_indices]
            number_cycle_pdv_samples = len(this_cycle_timebase_seconds)

            # numpy uses the k/theta and G.8263 uses alpha/beta notation for gamma distribution, so
            #   theta = 1 / beta
            #   k = alpha
            # but there is an error in G.8263 such that theta = beta
            #
            # Rec. ITU-T G.8263/Y.1363 (2012)/Amd.2 (05/2014), pp10
            # Add back the static offset
            pdv_noise_seconds = (
                self._random_generator.gamma(alpha, beta, size=number_cycle_pdv_samples)
                + rho
                + 57.32e-6
            )
            pdv_seconds[this_cycle_logical_indices] = pdv_noise_seconds

        return pdv_seconds

    def calculate_segment_bounds(
        self, timebase_seconds: numpy.array
    ) -> typing.Tuple[numpy.array, numpy.array]:
        number_segments = self.compute_noise_sequence_length(
            numpy.min(timebase_seconds), numpy.max(timebase_seconds)
        )

        start_times_seconds = numpy.zeros(number_segments)
        stop_times_seconds = numpy.zeros(number_segments)
        # Assume the timebase is uniformly sampled
        start_times_seconds[0] = timebase_seconds[0]

        k = 0
        while k < len(start_times_seconds):
            stop_times_seconds[k] = (
                start_times_seconds[k] + self._load_cycle_duration_seconds
            )

            if (stop_times_seconds[k] < numpy.max(timebase_seconds)) and (
                (k + 1) < len(start_times_seconds)
            ):
                start_times_seconds[k + 1] = numpy.min(
                    timebase_seconds[timebase_seconds > stop_times_seconds[k]]
                )

            k += 1

        return start_times_seconds, stop_times_seconds

    def compute_noise_sequence_length(self, min_time: float, max_time: float) -> int:
        # Assume the timebase is uniformly sampled
        noise_cycle_duration = self._load_cycle_duration_seconds

        number_cycle_samples = (
            math.floor((max_time - min_time) / noise_cycle_duration) + 1
        )

        return number_cycle_samples


def compute_load_flicker_noise_percent(
    input_noise: numpy.array, phi: numpy.array, theta: numpy.array
) -> numpy.array:
    assert len(phi) == len(theta)

    # Rec. ITU-T G.8263/Y.1363 (2012)/Amd.2 (05/2014), pp6, Eq. I-3
    M = len(phi)
    N = len(input_noise)
    Y = numpy.zeros((M, N))
    for n in range(1, N):
        Y[0, n] = (phi[0] * Y[0, (n - 1)]) + input_noise[n]

        for m in range(1, M):
            first_term = phi[m] * Y[m, (n - 1)]
            second_term = Y[(m - 1), n]
            third_term = -theta[m] * Y[(m - 1), (n - 1)]

            Y[m, n] = first_term + second_term + third_term

    flicker_noise = Y[-1, :]

    min_noise = numpy.min(flicker_noise)
    max_noise = numpy.max(flicker_noise)

    # Rec. ITU-T G.8263/Y.1363 (2012)/Amd.2 (05/2014), pp7, Eq. I-6
    load_flicker_noise_percent = (
        100 * (flicker_noise - min_noise) / (max_noise - min_noise)
    )

    assert numpy.all(load_flicker_noise_percent >= 0)
    assert numpy.all(load_flicker_noise_percent <= 100)

    return load_flicker_noise_percent


def compute_flicker_coefficients(
    r: float, m: int
) -> typing.Tuple[numpy.array, numpy.array]:
    # Rec. ITU-T G.8263/Y.1363 (2012)/Amd.2 (05/2014), pp6, Eq. I-5
    phi = numpy.zeros(m)
    phi[0] = 0.13

    omega = numpy.zeros(m)
    omega[0] = (1 - phi[0]) / math.sqrt(phi[0])
    omega[1:] = omega[0] / numpy.power(r, numpy.arange(1, m))

    mu = omega / r

    phi = 1 + mu * (mu - numpy.sqrt(numpy.power(mu, 2) + 4)) / 2

    theta = 1 + omega * (omega - numpy.sqrt(numpy.power(omega, 2) + 4)) / 2

    return phi, theta


def compute_gamma_coefficients(
    network_load_percent: numpy.array,
) -> typing.Tuple[numpy.array, numpy.array, numpy.array]:
    assert numpy.all(network_load_percent >= 0)
    assert numpy.all(network_load_percent <= 100)
    # Rec. ITU-T G.8263/Y.1363 (2012)/Amd.2 (05/2014), pp10
    # Table I.2, pp10
    alpha_coefficients = numpy.array(
        [
            3.0302171048327e-10,
            -9.7822643361772e-08,
            1.1854660981753e-05,
            -6.6624332958641e-04,
            1.8713517871851e-02,
            -1.4120879264166e-01,
            1.3306420437613e00,
        ]
    )
    beta_coefficients = numpy.array(
        [
            -3.7527709385196e-16,
            1.2590219237780e-13,
            -1.6595170368502e-11,
            1.0886566230108e-09,
            -3.7186572402355e-08,
            5.9390899042069e-07,
            1.6110589771449e-06,
        ]
    )
    rho_coefficients = numpy.array(
        [
            1.0843935243576e-15,
            -2.8578719666972e-13,
            2.9508400604002e-11,
            -1.4410536532614e-09,
            3.3119857891960e-08,
            -2.9200865252098e-07,
            8.1781119355525e-07,
        ]
    )

    assert len(alpha_coefficients) == 7
    assert len(beta_coefficients) == len(alpha_coefficients)
    assert len(rho_coefficients) == len(alpha_coefficients)

    alpha = numpy.zeros(network_load_percent.shape)
    beta = numpy.zeros(network_load_percent.shape)
    rho = numpy.zeros(network_load_percent.shape)

    clip_indices = network_load_percent > 99

    alpha[clip_indices] = 2.0132036140218e01
    beta[clip_indices] = 2.96693980102245e-06
    rho[clip_indices] = 5.59439990063761e-05

    interpolate_indices = network_load_percent <= 99
    interpolating_network_load = network_load_percent[interpolate_indices]

    alpha[interpolate_indices] = numpy.polyval(
        alpha_coefficients, interpolating_network_load
    )
    beta[interpolate_indices] = numpy.polyval(
        beta_coefficients, interpolating_network_load
    )
    rho[interpolate_indices] = numpy.polyval(
        rho_coefficients, interpolating_network_load
    )

    return alpha, beta, rho
