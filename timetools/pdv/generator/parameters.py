#
# Copyright 2017 Russell Smiley
#
# This file is part of timetools.
#
# timetools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# timetools is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with timetools.  If not, see <http://www.gnu.org/licenses/>.
#

import typing

import numpy.random as nr


class GaussianParameters:
    def __init__(
        self,
        mean: float = 0,
        standard_deviation: float = 1,
        lower_threshold: typing.Optional[float] = None,
        upper_threshold: typing.Optional[float] = None,
        random_seed: typing.Optional[int] = None,
    ):
        self.mean = mean
        self.standard_deviation = standard_deviation
        self.upper_threshold = upper_threshold
        self.lower_threshold = lower_threshold

        self.random_generator = None
        if random_seed is not None:
            # Assume randomSeed is a 32 bit integer
            self.random_generator = nr.RandomState(random_seed)
        else:
            self.random_generator = nr.RandomState()


class UniformParameters:
    def __init__(
        self,
        lower_threshold: float = 0,
        upper_threshold: float = 1,
        random_seed: typing.Optional[int] = None,
    ):
        self.upper_threshold = upper_threshold
        self.lower_threshold = lower_threshold

        self.random_generator = None
        if random_seed is not None:
            # Assume randomSeed is a 32 bit integer
            self.random_generator = nr.RandomState(random_seed)
        else:
            self.random_generator = nr.RandomState()


class ExponentialParameters:
    def __init__(
        self,
        offset: float = 0,
        scale: float = 1,
        upper_threshold: typing.Optional[float] = None,
        random_seed: typing.Optional[int] = None,
    ):
        self.offset = offset
        self.scale = scale
        self.upper_threshold = upper_threshold

        self.random_generator = None
        if random_seed is not None:
            # Assume randomSeed is a 32 bit integer
            self.random_generator = nr.RandomState(random_seed)
        else:
            self.random_generator = nr.RandomState()
