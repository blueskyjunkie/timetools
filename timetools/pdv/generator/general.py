#
# Copyright 2017 Russell Smiley
#
# This file is part of timetools.
#
# timetools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# timetools is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with timetools.  If not, see <http://www.gnu.org/licenses/>.
#

import numpy

from .parameters import ExponentialParameters, GaussianParameters, UniformParameters


class Predefined:
    def __init__(self, pdv_data: numpy.array):
        self.__pdv_data = pdv_data
        self.__last_iteration_offset = 0

    def generate(self, timebase_seconds: numpy.array) -> numpy.array:
        # PDV data is applied like a ring buffer against iterations of time bases.
        offset_indices = (
            numpy.arange(0, len(timebase_seconds)) + self.__last_iteration_offset
        )
        ring_indices = numpy.mod(offset_indices, len(self.__pdv_data))

        self.__last_iteration_offset += len(timebase_seconds)

        return self.__pdv_data[ring_indices]


class Gaussian:
    def __init__(self, parameters: GaussianParameters):
        self._mean = parameters.mean
        self.__standard_deviation = parameters.standard_deviation
        self.__lower_threshold = parameters.lower_threshold
        self.__upper_threshold = parameters.upper_threshold
        self.__random_generator = parameters.random_generator

    def generate(self, timebase_seconds: numpy.array) -> numpy.array:
        number_samples = len(timebase_seconds)

        this_pdv = self.__random_generator.normal(
            self._mean, self.__standard_deviation, number_samples
        )

        if self.__upper_threshold != None:
            this_pdv[this_pdv > self.__upper_threshold] = self.__upper_threshold

        if self.__lower_threshold != None:
            this_pdv[this_pdv < self.__lower_threshold] = self.__lower_threshold

        return this_pdv


class Uniform:
    def __init__(self, parameters: UniformParameters):
        self.__lower_threshold = parameters.lower_threshold
        self.__upper_threshold = parameters.upper_threshold
        self.__random_generator = parameters.random_generator

    def generate(self, timebase_seconds: numpy.array) -> numpy.array:
        number_samples = len(timebase_seconds)

        this_pdv = self.__random_generator.uniform(
            self.__lower_threshold, self.__upper_threshold, number_samples
        )

        return this_pdv


class Exponential:
    def __init__(self, parameters: ExponentialParameters):
        self.__offset = parameters.offset
        self.__scale = parameters.scale
        self.__upper_threshold = parameters.upper_threshold
        self.__random_generator = parameters.random_generator

    def generate(self, timebase_seconds: numpy.array) -> numpy.array:
        number_samples = len(timebase_seconds)

        this_pdv = (
            self.__random_generator.exponential(self.__scale, number_samples)
            + self.__offset
        )

        if self.__upper_threshold != None:
            this_pdv[this_pdv > self.__upper_threshold] = self.__upper_threshold

        return this_pdv
