#
# Copyright 2017 Russell Smiley
#
# This file is part of timetools.
#
# timetools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# timetools is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with timetools.  If not, see <http://www.gnu.org/licenses/>.
#

"""Define a concept for managing a time series signal; managing both the time base (x-axis time)
and the signal."""

import typing

import numpy

from ..exceptions import SignalProcessingError


class Chronology:
    """Manage a time series signal."""

    def __init__(
        self,
        timebase: numpy.array = numpy.array([]),
        signal: numpy.array = numpy.array([]),
    ):
        if len(timebase) != len(signal):
            raise SignalProcessingError(
                "Chronology timebase and signals must be equal lengths"
            )

        # timebase must be monotonic
        delta = numpy.diff(timebase)
        if not numpy.all(delta >= 0):
            raise SignalProcessingError(
                "Chronology timebase is not monotonically increasing"
            )

        self.timebase = timebase
        self.signal = signal

    def __len__(self):
        assert len(self.timebase) == len(self.signal)

        return len(self.timebase)

    def __lt__(self, other_chronology: "Chronology") -> "Chronology":
        """
        Element-wise comparison for signal values less than where the time bases of
        the two chronologies are the same.
        
        :param other_chronology: 
        
        :return: Chronology of resulting comparison.
        """
        (
            self_logical_indices,
            other_logical_indices,
        ) = self.__cross_match_logical_indices(other_chronology.timebase)

        data_result = numpy.less(
            self.signal[self_logical_indices],
            other_chronology.signal[other_logical_indices],
        )

        return Chronology(self.timebase[self_logical_indices], data_result)

    def __le__(self, other_chronology: "Chronology") -> "Chronology":
        """
        Element-wise comparison for signal values less than or equal where the time bases of
        the two chronologies are the same.

        :param other_chronology:

        :return: Chronology of resulting comparison.
        """
        (
            self_logical_indices,
            other_logical_indices,
        ) = self.__cross_match_logical_indices(other_chronology.timebase)

        data_result = numpy.less_equal(
            self.signal[self_logical_indices],
            other_chronology.signal[other_logical_indices],
        )

        return Chronology(self.timebase[self_logical_indices], data_result)

    def __gt__(self, other_chronology: "Chronology") -> "Chronology":
        """
        Element-wise comparison for signal values greater than where the time bases of
        the two chronologies are the same.

        :param other_chronology:

        :return: Chronology of resulting comparison.
        """
        (
            self_logical_indices,
            other_logical_indices,
        ) = self.__cross_match_logical_indices(other_chronology.timebase)

        data_result = numpy.greater(
            self.signal[self_logical_indices],
            other_chronology.signal[other_logical_indices],
        )

        return Chronology(self.timebase[self_logical_indices], data_result)

    def __ge__(self, other_chronology: "Chronology") -> "Chronology":
        """
        Element-wise comparison for signal values greater than or equal where the time bases of
        the two chronologies are the same.

        :param other_chronology:

        :return: Chronology of resulting comparison.
        """
        (
            self_logical_indices,
            other_logical_indices,
        ) = self.__cross_match_logical_indices(other_chronology.timebase)

        data_result = numpy.greater_equal(
            self.signal[self_logical_indices],
            other_chronology.signal[other_logical_indices],
        )

        return Chronology(self.timebase[self_logical_indices], data_result)

    def __eq__(self, other_chronology: "Chronology") -> "Chronology":
        """
        Element-wise comparison for signal values equal where the time bases of the two
        chronologies are the same.

        :param other_chronology:

        :return: Chronology of resulting comparison.
        """
        (
            self_logical_indices,
            other_logical_indices,
        ) = self.__cross_match_logical_indices(other_chronology.timebase)

        data_result = numpy.equal(
            self.signal[self_logical_indices],
            other_chronology.signal[other_logical_indices],
        )

        return Chronology(self.timebase[self_logical_indices], data_result)

    def __ne__(self, other_chronology: "Chronology") -> "Chronology":
        """
        Element-wise comparison for signal values not equal where the time bases of the two
        chronologies are the same.

        :param other_chronology:

        :return: Chronology of resulting comparison.
        """
        (
            self_logical_indices,
            other_logical_indices,
        ) = self.__cross_match_logical_indices(other_chronology.timebase)

        data_result = numpy.not_equal(
            self.signal[self_logical_indices],
            other_chronology.signal[other_logical_indices],
        )

        return Chronology(self.timebase[self_logical_indices], data_result)

    def __add__(self, other_chronology: "Chronology") -> "Chronology":
        """
        Element-wise addition of signal values where the time bases of the two chronologies are
        equal.

        :param other_chronology:

        :return: Chronology of resulting comparison.
        """
        (
            self_logical_indices,
            other_logical_indices,
        ) = self.__cross_match_logical_indices(other_chronology.timebase)

        data_result = (
            self.signal[self_logical_indices]
            + other_chronology.signal[other_logical_indices]
        )

        return Chronology(self.timebase[self_logical_indices], data_result)

    def __sub__(self, other_chronology: "Chronology") -> "Chronology":
        """
        Element-wise subtraction of signal values where the time bases of the two chronologies
        are equal.

        :param other_chronology:

        :return: Chronology of resulting comparison.
        """
        (
            self_logical_indices,
            other_logical_indices,
        ) = self.__cross_match_logical_indices(other_chronology.timebase)

        data_result = (
            self.signal[self_logical_indices]
            - other_chronology.signal[other_logical_indices]
        )

        return Chronology(self.timebase[self_logical_indices], data_result)

    def __mul__(self, other_chronology: "Chronology") -> "Chronology":
        """
        Element-wise multiplication of signal values where the time bases of the two chronologies
        are equal.

        :param other_chronology:

        :return: Chronology of resulting comparison.
        """
        (
            self_logical_indices,
            other_logical_indices,
        ) = self.__cross_match_logical_indices(other_chronology.timebase)

        data_result = (
            self.signal[self_logical_indices]
            * other_chronology.signal[other_logical_indices]
        )

        return Chronology(self.timebase[self_logical_indices], data_result)

    def __div__(self, other_chronology: "Chronology") -> "Chronology":
        """
        Element-wise division of signal values where the time bases of the two chronologies are
        equal.

        :param other_chronology:

        :return: Chronology of resulting comparison.
        """
        (
            self_logical_indices,
            other_logical_indices,
        ) = self.__cross_match_logical_indices(other_chronology.timebase)

        data_result = (
            self.signal[self_logical_indices]
            / other_chronology.signal[other_logical_indices]
        )

        return Chronology(self.timebase[self_logical_indices], data_result)

    def __cross_match_logical_indices(
        self, other_timebase: numpy.array
    ) -> typing.Tuple[numpy.array, numpy.array]:
        self_logical_indices = logical_indices(self.timebase, other_timebase)
        other_logical_indices = logical_indices(other_timebase, self.timebase)

        return (self_logical_indices, other_logical_indices)


def logical_indices(
    self_timebase: numpy.array, other_timebase: numpy.array
) -> numpy.array:
    """
    Extract logical indices with respect to ``self_timebase`` where ``other_timebase`` is
    equal to ``self_timebase``.

    :param self_timebase:
    :param other_timebase:

    :return: Numpy array of logical indices.
    """
    self_logical_indices = numpy.zeros(len(self_timebase), dtype=bool)

    for x in range(0, len(self_timebase)):
        self_logical_indices[x] = numpy.any(other_timebase == self_timebase[x])

    return self_logical_indices
