#
# Copyright 2017 Russell Smiley
#
# This file is part of timetools.
#
# timetools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# timetools is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with timetools.  If not, see <http://www.gnu.org/licenses/>.
#

import math

from .chronology import Chronology


def decimate(input_chronology: Chronology, decimation_ratio: int) -> Chronology:
    """
    Decimate a time series signal.

    :param input_chronology: Signal to be decimated
    :param decimation_ratio:

    :return: Decimated time series signal
    """
    assert (decimation_ratio / math.floor(decimation_ratio)) == 1

    output_timebase = input_chronology.timebase[
        0 : len(input_chronology.timebase) : decimation_ratio
    ]
    output_signal = input_chronology.signal[
        0 : len(input_chronology.signal) : decimation_ratio
    ]

    return Chronology(output_timebase, output_signal)
