#
# Copyright 2017 Russell Smiley
#
# This file is part of timetools.
#
# timetools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# timetools is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with timetools.  If not, see <http://www.gnu.org/licenses/>.
#

import math
import time
import typing


def print_time(elapsed_time_seconds: float) -> str:
    """
    Convert an elapsed time in seconds to a string in hours, minutes and seconds. The seconds are
    shown to two decimal places.

    :param elapsed_time_seconds: Elapsed time to be converted to string.

    :return: Text string representation of elapsed time in hours, minutes and seconds.
    """
    time_text = ""
    if elapsed_time_seconds >= 3600:
        hrs = math.floor(elapsed_time_seconds / 3600)
        time_text += "{0} hrs, ".format(hrs)

        remainder = elapsed_time_seconds - (hrs * 3600)

        time_text += print_time(remainder)
    elif elapsed_time_seconds >= 60:
        minutes = math.floor(elapsed_time_seconds / 60)
        time_text += "{0} min, ".format(minutes)

        remainder = elapsed_time_seconds - (minutes * 60)

        time_text += print_time(remainder)
    else:
        time_text = "{0:.2f} s".format(elapsed_time_seconds)

    return time_text


class StopWatch:
    """Simple stop watch using """

    def __init__(self):
        self.__timers = [time.time()]

        self.__last_time = self.__timers[0]

    def show_elapsed(self, this_message: str) -> str:
        current_time = time.time()
        self.__timers.append(current_time)

        elapsed_time = current_time - self.__last_time
        elapsed_time_text = "{0} {1}".format(this_message, print_time(elapsed_time))

        self.__last_time = current_time

        return elapsed_time_text

    def show_total(self, this_message: str) -> str:
        current_time = time.time()
        elapsed_time = current_time - self.__timers[0]

        total_time_text = "{0} {1}".format(this_message, print_time(elapsed_time))

        return total_time_text

    def record_timer(self):
        self.__timers.append(time.time())

    def report_timers(self) -> typing.List[float]:
        return self.__timers
