#
# Copyright 2017 Russell Smiley
#
# This file is part of timetools.
#
# timetools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# timetools is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with timetools.  If not, see <http://www.gnu.org/licenses/>.
#

import numpy


def upsample(sequence: numpy.array, upsampling_ratio: int) -> numpy.array:
    """
    Up sample a data sequence by some integer ratio by repeating existing data.

    For N samples in the original array and M oversampling ratio, the
    up sampled data has size N * M.

    :param sequence:
    :param upsampling_ratio:

    :return: Up sampled signal.
    """
    # Ensure the upsampling_ratio is an integer
    this_upsampling_ratio = int(upsampling_ratio)

    new_number_samples = len(sequence) * this_upsampling_ratio

    new_sequence = numpy.zeros(new_number_samples)
    for k in range(0, this_upsampling_ratio):
        xnew = numpy.arange(k, new_number_samples, this_upsampling_ratio)
        new_sequence[xnew] = sequence

    return new_sequence
