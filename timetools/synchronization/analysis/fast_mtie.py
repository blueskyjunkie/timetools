#
# Copyright 2017 Russell Smiley
#
# This file is part of timetools.
#
# timetools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# timetools is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with timetools.  If not, see <http://www.gnu.org/licenses/>.
#

import multiprocessing as mtp
from operator import itemgetter

import numpy

from timetools.signal_processing.job_queue import NoDaemonPool
from timetools.synchronization.analysis.itut_g810 import calculate_time_error

from ..intervals import generate_log_interval_scale, generate_monotonic_log_scale
from .mtie_kernels import smart_window


def calculate_mtie(
    local_time: numpy.array,
    reference_time: numpy.array,
    sampling_interval: float,
    desired_number_observations: int,
    maximum_number_workers: int = mtp.cpu_count(),
):
    assert len(local_time) > 1
    assert len(reference_time) > 1
    assert local_time.size == reference_time.size

    time_error = calculate_time_error(local_time, reference_time)

    max_interval_index = len(local_time) - 1

    interval_index = generate_monotonic_log_scale(
        numpy.floor(
            generate_log_interval_scale(
                1, max_interval_index, desired_number_observations
            )
        )
    )

    if maximum_number_workers == 1:
        mtie = _calculate_single_process_mtie(time_error, interval_index)
    else:
        needed_number_workers = min([maximum_number_workers, len(interval_index)])
        mtie = _calculate_multiprocess_mtie(
            time_error, interval_index, needed_number_workers
        )

    observation_intervals = sampling_interval * interval_index

    return (mtie, observation_intervals)


class MtieWorker:
    def __init__(self, time_error: numpy.array, kernel=smart_window):
        self.__time_error = time_error
        self.__kernel = kernel

    def __call__(self, this_interval):
        this_mtie = self.__kernel(self.__time_error, this_interval)

        return (this_mtie, this_interval)


def _calculate_multiprocess_mtie(
    time_error: numpy.array, interval_index: numpy.array, needed_number_workers: int
):
    number_intervals = len(interval_index)

    this_mtie_worker = MtieWorker(time_error)

    this_pool = NoDaemonPool(needed_number_workers)
    mtie_results = this_pool.map(this_mtie_worker, interval_index)

    assert len(mtie_results) == number_intervals

    mtie_results.sort(key=itemgetter(1))
    mtie, sorted_interval_results = map(list, zip(*mtie_results))

    assert numpy.all(numpy.equal(sorted_interval_results, interval_index))

    return numpy.array(mtie)


def _calculate_single_process_mtie(
    time_error: numpy.array, interval_index: numpy.array
):
    """
    Calculate Maximum Time Interval Error (MTIE) of two timebases using a 
    window step method, assuming they are identically, uniformly sampled.
    
    This is much faster to compute than the direct method.
    
    Assume localTime and referenceTime are numpy arrays.
    """
    number_intervals = len(interval_index)

    mtie = numpy.zeros(number_intervals)
    for k in numpy.arange(0, number_intervals):
        this_interval = interval_index[k]

        mtie[k] = smart_window(time_error, this_interval)

    return numpy.array(mtie)
