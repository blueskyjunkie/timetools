#
# Copyright 2017 Russell Smiley
#
# This file is part of timetools.
#
# timetools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# timetools is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with timetools.  If not, see <http://www.gnu.org/licenses/>.
#

import math
import typing

import numpy

import timetools.synchronization.intervals as si

from .mtie_kernels import sliding_window
from .tdev_kernels import mean


def calculate_time_error(local_time: numpy.array, reference_time: numpy.array):
    """
    Calculate Time Error (TE) of two time bases assuming they are identically, uniformly sampled
    and the same physical units (say, seconds).

    Assume ``local_time`` and ``reference_time`` are numpy arrays.

    "Definitions and terminology for synchronization networks", ITU-T G.810 (08/96),
    Section 4.5.13, pp7

    :param local_time:
    :param reference_time:

    :return:
    """
    assert local_time.size == reference_time.size

    return local_time - reference_time


def calculateTimeIntervalError(localTime, referenceTime, intervalSamples):
    """
    Calculate Time Interval Error (TIE) of two timebases assuming they are identically, uniformly
    sampled.

    Assume localTime and referenceTime are numpy arrays.

    "Definitions and terminology for synchronization networks", ITU-T G.810 (08/96),
    Section 4.5.14, pp7

    :param localTime:
    :param referenceTime:
    :param intervalSamples:

    :return:
    """
    assert localTime.size == referenceTime.size
    timeError = calculate_time_error(localTime, referenceTime)

    return (
        timeError[intervalSamples : timeError.size : 1]
        - timeError[0 : (timeError.size - intervalSamples) : 1]
    )


def calculate_mtie(
    local_time: numpy.array,
    reference_time: numpy.array,
    sampling_interval: float,
    desired_number_observations: int,
) -> typing.Tuple[numpy.array, numpy.array]:
    """
    Calculate Maximum Time Interval Error (MTIE) of two timebases using the 
    direct method, assuming they are identically, uniformly sampled.
    
    "Definitions and terminology for synchronization networks", ITU-T G.810 (08/96),
    Section 4.5.15, pp7
    
    The ITU definition does a direct calculation on all windows sizes, but here a logarithmic
    scale is used
    to improve computational efficiency slightly.
    
    Assume localTime and referenceTime are numpy arrays.
    """
    assert len(local_time) > 1
    assert len(reference_time) > 1
    assert local_time.size == reference_time.size

    time_error = calculate_time_error(local_time, reference_time)

    max_interval_index = len(local_time) - 1

    interval_index = si.generate_monotonic_log_scale(
        numpy.floor(
            si.generate_log_interval_scale(
                1, max_interval_index, desired_number_observations
            )
        )
    )
    mtie = numpy.zeros(interval_index.size)

    for k in numpy.arange(0, len(interval_index)):
        this_interval = interval_index[k]

        mtie[k] = sliding_window(time_error, this_interval)

    observation_intervals = sampling_interval * interval_index

    return (mtie, observation_intervals)


def calculate_tdev(
    local_time: numpy.array,
    reference_time: numpy.array,
    sampling_interval: float,
    desired_number_observations: int,
    kernel=mean,
) -> typing.Tuple[numpy.array, numpy.array]:
    assert len(local_time) > 1
    assert len(reference_time) > 1
    assert reference_time.size == local_time.size

    # Assume the signal is uniformly sampled.
    time_error = calculate_time_error(local_time, reference_time)

    number_error_points = len(time_error)
    raw_tdev_size = math.floor(number_error_points / 3)
    max_interval_index = raw_tdev_size

    interval_index = si.generate_monotonic_log_scale(
        numpy.floor(
            si.generate_log_interval_scale(
                1, max_interval_index, desired_number_observations
            )
        )
    )

    # ITU-T Rec. G.810 (08/96) pp 17
    raw_tdev = numpy.zeros(raw_tdev_size)
    for n in numpy.arange(0, raw_tdev_size) + 1:
        iteration_size = number_error_points - (3 * n) + 1
        tdev_factor = 1 / (6 * iteration_size)

        tdev_sum = 0
        for j in numpy.arange(0, iteration_size) + 1:
            tdev_sum += numpy.power(kernel(time_error, n, j), 2)

        raw_tdev[n - 1] = math.sqrt(tdev_factor * tdev_sum)

    # Reduce the resolution to that specified
    # In the case of the direct TDEV method, this doesn't improve computation time, but it does
    # improve memory usage
    tdev = raw_tdev[interval_index - 1]

    observation_intervals = sampling_interval * interval_index

    return (tdev, observation_intervals)


def adev_kernel(time_error: numpy.array, n: int) -> numpy.array:
    number_points = len(time_error)
    base_index = numpy.arange(0, (number_points - (2 * n)))

    error1 = time_error[base_index + (2 * n)]
    error2 = 2 * time_error[base_index + n]
    error3 = time_error[base_index]

    this_sum = numpy.sum(error1 - error2 + error3)

    return this_sum


def calculate_adev(
    local_time: numpy.array,
    reference_time: numpy.array,
    sampling_interval: float,
    desired_number_observations: int,
) -> typing.Tuple[numpy.array, numpy.array]:
    assert len(local_time) > 1
    assert len(reference_time) > 1
    assert reference_time.size == local_time.size

    # Assume the signal is uniformly sampled.
    time_error = calculate_time_error(local_time, reference_time)

    number_error_points = len(time_error)
    raw_adev_size = math.floor((number_error_points - 1) / 2)
    max_interval_index = raw_adev_size

    interval_index = si.generate_monotonic_log_scale(
        numpy.floor(
            si.generate_log_interval_scale(
                1, max_interval_index, desired_number_observations
            )
        )
    )

    # ITU-T Rec. G.810 (08/96), Appendix II.1, pp 14
    raw_adev = numpy.zeros(raw_adev_size)
    for n in numpy.arange(0, raw_adev_size) + 1:
        iteration_size = number_error_points - (2 * n)
        adev_factor = 1 / (2 * numpy.power((n * sampling_interval), 2) * iteration_size)

        adev_sum = numpy.power(adev_kernel(time_error, n), 2)

        raw_adev[n - 1] = math.sqrt(adev_factor * adev_sum)

    # Reduce the resolution to that specified
    # In the case of the direct ADEV method, this doesn't improve computation time, but it does
    # improve memory usage
    adev = raw_adev[interval_index - 1]

    observation_intervals = sampling_interval * interval_index

    return (adev, observation_intervals)


def calculate_mdev(
    local_time: numpy.array,
    reference_time: numpy.array,
    sampling_interval: float,
    desired_number_observations: int,
) -> typing.Tuple[numpy.array, numpy.array]:
    assert len(local_time) > 1
    assert len(reference_time) > 1
    assert reference_time.size == local_time.size

    tdev, observation_intervals = calculate_tdev(
        local_time, reference_time, sampling_interval, desired_number_observations
    )

    # ITU-T Rec. G.810 (08/96), Appendix II.2, pp 15
    # ITU-T Rec. G.810 (08/96), Appendix II.3, pp 16
    mdev = (numpy.sqrt(3) / observation_intervals) * tdev

    return (mdev, observation_intervals)


def tie_rms_kernel(time_error: numpy.array, n: int) -> numpy.array:
    number_points = len(time_error)
    base_index = numpy.arange(0, (number_points - n))

    error1 = time_error[base_index + n]
    error2 = time_error[base_index]

    this_sum = numpy.sum(numpy.power((error1 - error2), 2))

    return this_sum


def calculate_tie_rms(
    local_time: numpy.array,
    reference_time: numpy.array,
    sampling_interval: float,
    desired_number_observations: int,
) -> typing.Tuple[numpy.array, numpy.array]:
    assert len(local_time) > 1
    assert len(reference_time) > 1
    assert reference_time.size == local_time.size

    # Assume the signal is uniformly sampled.
    time_error = calculate_time_error(local_time, reference_time)

    number_error_points = len(time_error)
    raw_tie_rms_size = number_error_points - 1
    max_interval_index = raw_tie_rms_size

    interval_index = si.generate_monotonic_log_scale(
        numpy.floor(
            si.generate_log_interval_scale(
                1, max_interval_index, desired_number_observations
            )
        )
    )

    # ITU-T Rec. G.810 (08/96), Appendix II.4, pp 18
    raw_tie_rms = numpy.zeros(raw_tie_rms_size)
    for n in numpy.arange(0, raw_tie_rms_size) + 1:
        iteration_size = number_error_points - n
        tie_rms_factor = 1 / iteration_size

        tie_rms_sum = tie_rms_kernel(time_error, n)

        raw_tie_rms[n - 1] = math.sqrt(tie_rms_factor * tie_rms_sum)

    # Reduce the resolution to that specified
    # In the case of the direct TIE rms method, this doesn't improve computation time,
    # but it does improve memory usage
    tie_rms = raw_tie_rms[interval_index - 1]

    observation_intervals = sampling_interval * interval_index

    return (tie_rms, observation_intervals)


def calculate_tvar(
    local_time: numpy.array,
    reference_time: numpy.array,
    sampling_interval: float,
    desired_number_observations: int,
) -> typing.Tuple[numpy.array, numpy.array]:
    tdev, observation_intervals = calculate_tdev(
        local_time, reference_time, sampling_interval, desired_number_observations
    )

    tvar = numpy.power(tdev, 2)

    return (tvar, observation_intervals)
