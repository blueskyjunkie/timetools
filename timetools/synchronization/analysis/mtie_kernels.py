#
# Copyright 2017 Russell Smiley
#
# This file is part of timetools.
#
# timetools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# timetools is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with timetools.  If not, see <http://www.gnu.org/licenses/>.
#

import numpy


def _calculate_peak_to_peak_amplitude(signal: numpy.array) -> float:
    max_error = numpy.max(signal)
    min_error = numpy.min(signal)

    peak_to_peak_amplitude = max_error - min_error

    return peak_to_peak_amplitude


def sliding_window(time_error: numpy.array, window_size: int) -> numpy.array:
    """
    Calculate MTIE using a sliding window.

    Slide a window along the time error signal, sample by sample calculating the peak to peak
    error within each window.

    :param time_error: Time error signal to analyse.
    :param window_size: Window size in number of samples.

    :return: Maximum Time Interval Error (MTIE) of time error signal.
    """
    this_mtie = 0
    for m in numpy.arange(0, (time_error.size - window_size), 1):
        this_window = time_error[m : (m + window_size + 1) : 1]
        peak_to_peak_error = _calculate_peak_to_peak_amplitude(this_window)

        if peak_to_peak_error > this_mtie:
            this_mtie = peak_to_peak_error

    return this_mtie


def smart_window(time_error: numpy.array, window_size: int):
    """
    Calculate MTIE using a dynamic window selection method.

    :param time_error: Time error signal to analyse.
    :param window_size: Window size in number of samples.

    :return: Maximum Time Interval Error (MTIE) of time error signal.
    """

    def find_maximum_position(this_window: numpy.array, m):
        max_window_position = numpy.argmax(this_window)

        max_signal_position = max_window_position + m

        return max_signal_position

    def find_minimum_position(this_window: numpy.array, m):
        min_window_position = numpy.argmin(this_window)

        min_signal_position = min_window_position + m

        return min_signal_position

    number_samples = time_error.size

    this_mtie = 0

    m = 0
    completed = False
    last_iteration = False
    while not completed:
        this_window = time_error[m : (m + window_size + 1) : 1]
        peak_to_peak_error = _calculate_peak_to_peak_amplitude(this_window)

        if peak_to_peak_error > this_mtie:
            this_mtie = peak_to_peak_error

        max_signal_position = find_maximum_position(this_window, m)
        min_signal_position = find_minimum_position(this_window, m)

        next_window_position = numpy.min([max_signal_position, min_signal_position])

        if last_iteration:
            completed = True
        elif next_window_position == m:
            next_window_position = m + 1

        if (next_window_position + window_size) >= number_samples:
            next_window_position = number_samples - window_size
            last_iteration = True

        m = next_window_position

    return this_mtie
