#
# Copyright 2017 Russell Smiley
#
# This file is part of timetools.
#
# timetools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# timetools is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with timetools.  If not, see <http://www.gnu.org/licenses/>.
#

import math
import multiprocessing as mtp
import typing
from operator import itemgetter

import numpy

import timetools.signal_processing.job_queue as tsj
from timetools.synchronization.analysis.itut_g810 import calculate_time_error
from timetools.synchronization.analysis.tdev_kernels import mean
from timetools.synchronization.intervals import (
    generate_log_interval_scale,
    generate_monotonic_log_scale,
)


def calculate_tdev(
    local_time: numpy.array,
    reference_time: numpy.array,
    sampling_interval: float,
    desired_number_observations: int,
    maximum_number_workers=mtp.cpu_count(),
) -> typing.Tuple[numpy.array, numpy.array]:
    assert len(local_time) > 1
    assert len(reference_time) > 1
    assert local_time.size == reference_time.size

    # Assume the signal is uniformly sampled.
    time_error = calculate_time_error(local_time, reference_time)

    number_error_points = len(time_error)
    raw_tdev_size = math.floor(number_error_points / 3.0)
    max_interval_index = raw_tdev_size

    interval_index = generate_monotonic_log_scale(
        numpy.floor(
            generate_log_interval_scale(
                1, max_interval_index, desired_number_observations
            )
        )
    )

    if maximum_number_workers == 1:
        tdev = _calculate_single_process_tdev(time_error, interval_index, raw_tdev_size)
    else:
        needed_number_workers = min([maximum_number_workers, len(interval_index)])
        tdev = _calculate_multiprocessdev(
            time_error,
            interval_index,
            raw_tdev_size,
            maximum_number_workers=needed_number_workers,
        )

    observation_intervals = sampling_interval * interval_index

    return (tdev, observation_intervals)


def _calculate_single_process_tdev(
    time_error: numpy.array, interval_index: numpy.array, raw_tdev_size: int
) -> numpy.array:
    """
    For single process, a faster TDEV calculation than the G.810 direct method.
    """
    number_error_points = len(time_error)

    # "RAPID CALCULATION OF TIME DEVIATION AND MODIFIED ALLAN DEVIATION FOR
    # CHARACTERIZING TELECOMMUNICATIONS CLOCK STABILITY",
    #    Li, Liao, Hwang, International Journal of Computers and Applications,
    #    Vol. 30, No. 2, 2008

    # Interval size n = 1 must be the first observation interval size
    j1_array = numpy.arange(0, (number_error_points - 2))

    delta1j = (
        time_error[j1_array]
        - (2.0 * time_error[j1_array + 1])
        + time_error[j1_array + 2]
    )
    # s(0, j) = 0, pp94
    s0j = 0
    snj = s0j + delta1j
    y = numpy.power(snj[j1_array], 2, dtype="float64") / (
        6.0 * (number_error_points - 2)
    )
    x = numpy.sum(y)

    raw_tdev = numpy.zeros(raw_tdev_size, dtype="float64")
    raw_tdev[0] = math.sqrt(x)

    # Calculate the full resolution (sample by sample) TDEV
    for n in numpy.arange(1, raw_tdev_size) + 1:
        nj_array = numpy.arange(0, (number_error_points - (3 * n) + 1))

        deltanj = (
            3.0
            * (
                time_error[n - 1 + nj_array]
                - time_error[(2 * n) - 2 + nj_array]
                - time_error[(2 * n) - 1 + nj_array]
            )
        ) + (
            time_error[(3 * n) - 3 + nj_array]
            + time_error[(3 * n) - 2 + nj_array]
            + time_error[(3 * n) - 1 + nj_array]
        )

        snj[nj_array] += deltanj[nj_array]
        y = numpy.power(snj[nj_array], 2, dtype="float64") / (
            (6.0 * n * n) * (number_error_points - (3.0 * n) + 1)
        )
        x = numpy.sum(y)

        raw_tdev[n - 1] = numpy.sqrt(x)

    # Reduce the resolution to that specified by the user
    tdev = raw_tdev[interval_index - 1]

    return tdev


class TdevWorker:
    def __init__(self, time_error: numpy.array, kernel=mean):
        self.__time_error = time_error
        self.__kernel = kernel

    def __call__(self, this_interval):
        iteration_size = self.__time_error.size - (3 * this_interval) + 1
        tdev_factor = 1 / (6.0 * iteration_size)

        tdev_sum = 0
        for j in numpy.arange(0, iteration_size) + 1:
            tdev_sum += numpy.power(
                self.__kernel(self.__time_error, this_interval, j), 2, dtype="float64"
            )

        this_tdev = numpy.sqrt(tdev_factor * tdev_sum)

        return (this_tdev, this_interval)


def _calculate_multiprocessdev(
    time_error: numpy.array,
    interval_index: numpy.array,
    raw_tdev_size: int,
    kernel=mean,
    maximum_number_workers=mtp.cpu_count(),
) -> numpy.array:
    """
    Use the direct TDEV kernel, but take advantage of the embarrassingly 
    parallel nature of the iteration over intervals.
    """
    this_tdev_worker = TdevWorker(time_error, kernel=kernel)

    needed_number_workers = min([maximum_number_workers, len(interval_index)])
    this_pool = tsj.NoDaemonPool(needed_number_workers)
    tdev_results = this_pool.map(this_tdev_worker, interval_index)

    tdev_results.sort(key=itemgetter(1))

    total_tdev_list, total_sorted_interval_results_list = map(list, zip(*tdev_results))
    total_tdev = numpy.array(total_tdev_list, dtype="float64")
    total_sorted_interval_results = numpy.array(
        total_sorted_interval_results_list, dtype="int64"
    )
    assert len(total_tdev) == len(total_sorted_interval_results)

    indexing = [
        i
        for i in numpy.arange(0, len(total_sorted_interval_results))
        for item in interval_index
        if total_sorted_interval_results[i] == item
    ]
    selected_tdev = total_tdev[indexing]
    selected_interval_results = total_sorted_interval_results[indexing]

    assert len(tdev_results) == len(interval_index)
    assert all(selected_interval_results == interval_index)

    return numpy.array(selected_tdev)
