#
# Copyright 2017 Russell Smiley
#
# This file is part of timetools.
#
# timetools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# timetools is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with timetools.  If not, see <http://www.gnu.org/licenses/>.
#

from ...analysis import Mask

MASK_BOUND_PPM = 4.6


def generate_ffo_mask_ppm(duration_seconds: float = (24 * 3600 * 365)) -> Mask:
    """
    Generate a +/- FFO mask over the specified duration. The FFO bounds are specified in PPM, per
    Rec. ITU-T G.8262/Y.1362 (07/2010), Section 6.2, pp3

    :param duration_seconds: Duration in seconds over which to generate the FFO bound.

    :return: Mask specification for the specified duration.
    """

    ffo_mask_ppm = Mask([([0, duration_seconds], [MASK_BOUND_PPM], [-MASK_BOUND_PPM])])

    return ffo_mask_ppm
