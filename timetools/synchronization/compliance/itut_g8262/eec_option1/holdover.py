#
# Copyright 2017 Russell Smiley
#
# This file is part of timetools.
#
# timetools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# timetools is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with timetools.  If not, see <http://www.gnu.org/licenses/>.
#

import numpy

from ...analysis import Mask

a1 = 50
a2 = 2000
b = 1.16e-4
c = 120

phase_error_polynomial_ns = numpy.array([c, (a1 + a2), (0.5 * b)])
ffo_polynomial_ppb = numpy.array([4.6e3])

# Rec. ITU-T G.8262/Y.1362 (07/2010), pp17
phase_error_ns = Mask(
    [([0], phase_error_polynomial_ns.tolist(), (-phase_error_polynomial_ns).tolist())]
)

ffo_ppb = Mask([([0], ffo_polynomial_ppb.tolist(), (-ffo_polynomial_ppb).tolist())])
