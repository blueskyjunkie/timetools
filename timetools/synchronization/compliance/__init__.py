#
# Copyright 2017 Russell Smiley
#
# This file is part of timetools.
#
# timetools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# timetools is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with timetools.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Synchronization compliance masks.

Exports the following masks:

   * ``g8261_1_calculate_cluster_peak``
   * ``g8261_1_calculate_floor_packet_percent``
   * ``g8261_1_evaluate_pdv_network_limits``
   * ``g8261_dc1_wander_budget``
   * ``g8261_dc2_wander_budget``
   * ``g8261_eec1_network_wander``
   * ``g8261_eec2_network_wander``
   * ``g8262_eec1_frequency_accuracy``
   * ``g8262_eec1_holdover``
   * ``g8262eec1_wander_generation``
   * ``g8262_eec1_wander_tolerance``
   * ``g8262_eec2_frequency_accuracy``
   * ``g8262_eec2_holdover``
   * ``g8262_eec2_noise_transfer``
   * ``g8262_eec2_phase_discontinuity``
   * ``g8262_eec2_transient``
   * ``g8262_eec2_wander_generation``
   * ``g8262_eec2_wander_tolerance``
   * ``g8262_eec2_wander_transfer``
   * ``g8262_jitter_generation``
   * ``g8262_jitter_tolerance``
   * ``analyze_itut_g8263_mask``
   * ``g8263_holdover_transient``
   * ``g8263_wander_generation``
"""

from .itut_g8261.deployment_case1 import wander_budget as g8261_dc1_wander_budget
from .itut_g8261.deployment_case2 import wander_budget as g8261_dc2_wander_budget
from .itut_g8261.eec_option1 import network_wander as g8261_eec1_network_wander
from .itut_g8261.eec_option2 import network_wander as g8261_eec2_network_wander
from .itut_g8261_1 import calculate_cluster_peak as g8261_1_calculate_cluster_peak
from .itut_g8261_1 import (
    calculate_floor_packet_percent as g8261_1_calculate_floor_packet_percent,
)
from .itut_g8261_1 import (
    evaluate_pdv_network_limits as g8261_1_evaluate_pdv_network_limits,
)
from .itut_g8262 import jitter_generation as g8262_jitter_generation
from .itut_g8262 import jitter_tolerance as g8262_jitter_tolerance
from .itut_g8262.eec_option1 import frequency_accuracy as g8262_eec1_frequency_accuracy
from .itut_g8262.eec_option1 import holdover as g8262_eec1_holdover
from .itut_g8262.eec_option1 import wander_generation as g8262_eec1_wander_generation
from .itut_g8262.eec_option1 import wander_tolerance as g8262_eec1_wander_tolerance
from .itut_g8262.eec_option2 import frequency_accuracy as g8262_eec2_frequency_accuracy
from .itut_g8262.eec_option2 import holdover as g8262_eec2_holdover
from .itut_g8262.eec_option2 import noise_transfer as g8262_eec2_noise_transfer
from .itut_g8262.eec_option2 import (
    phase_discontinuity as g8262_eec2_phase_discontinuity,
)
from .itut_g8262.eec_option2 import transient as g8262_eec2_transient
from .itut_g8262.eec_option2 import wander_generation as g8262_eec2_wander_generation
from .itut_g8262.eec_option2 import wander_tolerance as g8262_eec2_wander_tolerance
from .itut_g8262.eec_option2 import wander_transfer as g8262_eec2_wander_transfer
from .itut_g8263 import holdover_transient as g8263_holdover_transient
from .itut_g8263 import wander_generation as g8263_wander_generation
from .itut_g8263.compute import analyze_itut_g8263_mask
