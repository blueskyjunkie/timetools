#
# Copyright 2020 Russell Smiley
#
# This file is part of timetools.
#
# timetools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# timetools is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with timetools.  If not, see <http://www.gnu.org/licenses/>.
#

"""G.8261.1 PDV related calculations."""

import multiprocessing as mtp
import typing

import numpy

from timetools.signal_processing.job_queue import NoDaemonPool


def calculate_floor_packet_percent(
    pdv_microseconds: numpy.array, this_cluster_range_threshold_microseconds: float
) -> float:
    window_floor_delay_microseconds = numpy.min(pdv_microseconds)
    cluster_peak_microseconds = (
        window_floor_delay_microseconds + this_cluster_range_threshold_microseconds
    )

    floor_cluster_pdv = (pdv_microseconds >= window_floor_delay_microseconds) & (
        pdv_microseconds <= cluster_peak_microseconds
    )

    number_floor_cluster = floor_cluster_pdv.sum()

    number_window_elements = len(pdv_microseconds)

    floor_packet_percent = number_floor_cluster / number_window_elements * 100

    return floor_packet_percent


def calculate_cluster_peak(
    pdv_microseconds: numpy.array, this_floor_packet_percent_threshold: float
) -> float:
    window_floor_delay_microseconds = numpy.min(pdv_microseconds)

    number_window_elements = len(pdv_microseconds)
    number_cluster_packets = int(
        numpy.floor(
            (this_floor_packet_percent_threshold / 100) * number_window_elements
        )
    )

    sorted_unique_pdv_microseconds = numpy.unique(pdv_microseconds)

    cluster_threshold_pdv_microsecond = sorted_unique_pdv_microseconds[
        number_cluster_packets
    ]

    cluster_peak_pdv = (
        cluster_threshold_pdv_microsecond - window_floor_delay_microseconds
    )

    return cluster_peak_pdv


class Worker:
    def __init__(
        self,
        timebase_seconds: numpy.array,
        pdv_microseconds: numpy.array,
        floor_packet_percent_threshold: float,
        cluster_range_threshold_microseconds: float,
        window_duration_seconds: float,
    ):
        self.__timebase_seconds = timebase_seconds
        self.__pdv_microseconds = pdv_microseconds
        self.__floor_packet_percent_threshold = floor_packet_percent_threshold
        self.__cluster_range_threshold_microseconds = (
            cluster_range_threshold_microseconds
        )
        self.__window_duration_seconds = window_duration_seconds

    def __call__(self, this_interval_index: numpy.array):
        pdv_analysis = []
        for this_index, result_index in zip(
            this_interval_index, range(0, len(this_interval_index))
        ):
            lower_window_bound_seconds = self.__timebase_seconds[this_index]
            upper_window_bound_seconds = (
                lower_window_bound_seconds + self.__window_duration_seconds
            )
            window_time_index = (
                self.__timebase_seconds >= lower_window_bound_seconds
            ) & (self.__timebase_seconds < upper_window_bound_seconds)
            window_pdv_microseconds = self.__pdv_microseconds[window_time_index]

            calculated_floor_packet_percent = calculate_floor_packet_percent(
                window_pdv_microseconds, self.__cluster_range_threshold_microseconds
            )
            calculated_cluster_peak_microseconds = calculate_cluster_peak(
                window_pdv_microseconds, self.__floor_packet_percent_threshold
            )
            pdv_analysis.append(
                (calculated_floor_packet_percent, calculated_cluster_peak_microseconds)
            )

        return (this_interval_index, pdv_analysis)


def scatter_indices(index_array: numpy.array, number_groups: int):
    """
    Split the array of indices into groups for multiprocessing.

    Assume that the number of groups cannot necessarily be divided evenly across the array.

    :param index_array:
    :param number_groups:

    :return:
    """

    # numpy.array_split returns a float array so need to restore it to int for indexing.
    float_split_indices = numpy.array_split(index_array, number_groups)

    split_indices = [x.astype(int) for x in float_split_indices]

    return split_indices


def calculate_result_size(this_split_result: numpy.array) -> int:
    this_size = 0
    for this_result in this_split_result:
        this_size += len(this_result[0])

    return this_size


def gather_results(
    split_result_array: numpy.array,
) -> typing.Tuple[numpy.array, numpy.array]:
    """
        Assume splitResultArray is an iterable of tuples. The first element of
        tuples is the group of indices calculated. The second element is a
        result of arbitrary type.
    """
    array_size = calculate_result_size(split_result_array)

    result_array = [() for i in range(0, array_size)]
    ordered_index_array = numpy.zeros(array_size)
    for this_result in split_result_array:
        group_indices = this_result[0]
        group_results = this_result[1]

        for i in range(0, len(group_indices)):
            result_array[group_indices[i]] = group_results[i]

        ordered_index_array[group_indices] = group_indices

    return (result_array, ordered_index_array)


def evaluate_pdv_network_limits(
    measurement_time_seconds: numpy.array,
    pdv_magnitude_microseconds: numpy.array,
    needed_number_workers: int = mtp.cpu_count(),
) -> typing.Tuple[bool, bool, numpy.array, numpy.array]:
    window_duration_seconds = 200
    floor_packet_percent_threshold = 1
    cluster_range_threshold_microseconds = 150

    end_index = numpy.searchsorted(
        measurement_time_seconds,
        (measurement_time_seconds[-1] - window_duration_seconds),
    )

    indices = numpy.arange(0, end_index)

    split_indices = scatter_indices(indices, needed_number_workers)

    this_worker = Worker(
        measurement_time_seconds,
        pdv_magnitude_microseconds,
        floor_packet_percent_threshold,
        cluster_range_threshold_microseconds,
        window_duration_seconds,
    )

    this_pool = NoDaemonPool(needed_number_workers)
    split_results = this_pool.map(this_worker, split_indices)

    assert len(split_results) == len(split_indices)

    results, ordered_index = gather_results(split_results)

    assert len(results) == len(indices)
    assert len(ordered_index) == len(indices)
    assert numpy.all(indices == ordered_index)

    calculated_floor_packet_percent = numpy.array([x[0] for x in results])
    calculated_cluster_peak_microseconds = numpy.array([x[1] for x in results])

    return (
        numpy.all(calculated_floor_packet_percent >= floor_packet_percent_threshold),
        numpy.all(
            calculated_cluster_peak_microseconds < cluster_range_threshold_microseconds
        ),
        calculated_floor_packet_percent,
        calculated_cluster_peak_microseconds,
    )
