#
# Copyright 2017 Russell Smiley
#
# This file is part of timetools.
#
# timetools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# timetools is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with timetools.  If not, see <http://www.gnu.org/licenses/>.
#

import typing

import numpy

from timetools.synchronization.analysis.fast_mtie import calculate_mtie
from timetools.synchronization.compliance.analysis import Mask
from timetools.synchronization.compliance.itut_g8263.wander_generation import (
    constant_temperature_mtie_mask_ns,
)


def analyze_itut_g8263_mask(
    local_time_seconds: numpy.array,
    reference_time_seconds: numpy.array,
    sampling_interval_seconds: float,
    desired_number_observations: int,
) -> typing.Tuple[numpy.array, Mask, typing.Tuple[numpy.array, numpy.array]]:
    """

    :param local_time_seconds:
    :param reference_time_seconds:
    :param sampling_interval_seconds:
    :param desired_number_observations:

    :return:
    """
    mtie_seconds, observation_intervals_seconds = calculate_mtie(
        local_time_seconds,
        reference_time_seconds,
        sampling_interval_seconds,
        desired_number_observations,
    )

    mtie_nanoseconds = mtie_seconds / 1e-9

    analysis_result = constant_temperature_mtie_mask_ns.evaluate(
        (observation_intervals_seconds, mtie_nanoseconds)
    )

    return (
        analysis_result,
        constant_temperature_mtie_mask_ns,
        (observation_intervals_seconds, mtie_nanoseconds),
    )
