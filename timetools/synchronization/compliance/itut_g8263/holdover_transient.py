#
# Copyright 2017 Russell Smiley
#
# This file is part of timetools.
#
# timetools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# timetools is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with timetools.  If not, see <http://www.gnu.org/licenses/>.
#

from ..analysis import Mask
from .definitions import (
    transientResponseFfoPpb,
    transientResponseFfoRatePpbPerSecond,
    transientResponsePhaseErrorNs,
)

# Holdover transient response, Section 8.1, Rec. ITU-T G.8263/Y.1363 (02/2012), pp7
phase_error_mask_ns = Mask(
    [
        (
            [0],
            transientResponsePhaseErrorNs.tolist(),
            (-transientResponsePhaseErrorNs).tolist(),
        )
    ]
)

ffo_mask_ppb = Mask(
    [([0], transientResponseFfoPpb.tolist(), (-transientResponseFfoPpb).tolist(),)]
)

ffo_rate_mask_ppb_per_second = Mask(
    [
        (
            [0],
            transientResponseFfoRatePpbPerSecond.tolist(),
            (-transientResponseFfoRatePpbPerSecond).tolist(),
        )
    ]
)
