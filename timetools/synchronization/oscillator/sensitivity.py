#
# Copyright 2017 Russell Smiley
#
# This file is part of timetools.
#
# timetools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# timetools is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with timetools.  If not, see <http://www.gnu.org/licenses/>.
#

import abc

import numpy


class TemperatureSensitivityModelInterface(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def generate(self, reference_temperature_kelvin: numpy.array) -> numpy.array:
        ...


class LinearTemperatureSensitivity:
    def __init__(
        self,
        temperature_sensitivity_ppb_per_kelvin: float,
        reference_temperature_kelvin: float = 293.15,
    ):
        self.__reference_temperature_kelvin = reference_temperature_kelvin
        self.__sensitivity_ppb_per_kelvin = temperature_sensitivity_ppb_per_kelvin

    def generate(self, reference_temperature_kelvin: numpy.array) -> numpy.array:
        # Assume zero ppb offset at the reference temperature
        ffo_ppb = (
            reference_temperature_kelvin - self.__reference_temperature_kelvin
        ) * self.__sensitivity_ppb_per_kelvin

        return ffo_ppb
