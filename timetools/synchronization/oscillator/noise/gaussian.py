#
# Copyright 2017 Russell Smiley
#
# This file is part of timetools.
#
# timetools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# timetools is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with timetools.  If not, see <http://www.gnu.org/licenses/>.
#

"""Gaussian noise model for oscillator."""

from .model import NoiseModel, ReferenceTimeType, SeedType


class GaussianNoise(NoiseModel):
    """Gaussian noise model for oscillator."""

    def __init__(self, standard_deviation_ppb: float = 1, seed: SeedType = None):
        super().__init__(seed)
        self.__standard_deviation_ppb = standard_deviation_ppb

    def generate(self, reference_time_seconds: ReferenceTimeType):
        this_noise = self.random_state.normal(
            scale=self.__standard_deviation_ppb, size=reference_time_seconds.shape
        )

        return this_noise
