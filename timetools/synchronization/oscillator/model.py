#
# Copyright 2020 Russell Smiley
#
# This file is part of timetools.
#
# timetools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# timetools is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with timetools.  If not, see <http://www.gnu.org/licenses/>.
#

import logging
import typing

import numpy

from .aging import AgingModelInterface
from .noise import NoiseModel
from .sensitivity import TemperatureSensitivityModelInterface

log = logging.getLogger(__name__)


class OscillatorModel:
    def __init__(
        self,
        initial_ffo_ppb: float = 0,
        aging_model: typing.Optional[AgingModelInterface] = None,
        temperature_sensitivity_model: typing.Optional[
            TemperatureSensitivityModelInterface
        ] = None,
        noise_model: typing.Optional[NoiseModel] = None,
    ):
        self.initial_ffo_ppb = initial_ffo_ppb
        self.aging_model = aging_model
        self.temperature_sensitivity_model = temperature_sensitivity_model
        self.noise_model = noise_model

    def generate(
        self,
        reference_time_seconds: numpy.array,
        reference_temperature_kelvin: typing.Optional[numpy.array] = None,
    ) -> numpy.array:

        ffo_ppb = numpy.ones(reference_time_seconds.shape) * self.initial_ffo_ppb

        if self.aging_model is not None:
            ffo_ppb += self.aging_model.generate(reference_time_seconds)

        if self.noise_model is not None:
            ffo_ppb += self.noise_model.generate(reference_time_seconds)

        if (self.temperature_sensitivity_model is not None) and (
            reference_temperature_kelvin is not None
        ):
            assert numpy.all(
                numpy.equal(
                    reference_temperature_kelvin.shape,
                    reference_temperature_kelvin.shape,
                )
            )
            ffo_ppb += self.temperature_sensitivity_model.generate(
                reference_temperature_kelvin
            )

        elif (reference_temperature_kelvin is None) and (
            self.temperature_sensitivity_model is not None
        ):
            log.warning(
                "Temperature sensitivity model was specified but reference temperature not specified for FFO calculations."
            )

        elif (reference_temperature_kelvin is not None) and (
            self.temperature_sensitivity_model is None
        ):
            log.warning(
                "Temperature sensitivity model was not specified but reference temperature was specified for FFO calculations."
            )

        return ffo_ppb
