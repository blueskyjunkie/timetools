#
# Copyright 2017 Russell Smiley
#
# This file is part of timetools.
#
# timetools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# timetools is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with timetools.  If not, see <http://www.gnu.org/licenses/>.
#

import abc

import numpy


class AgingModelInterface(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def generate(self, reference_time_seconds: numpy.array) -> numpy.array:
        ...


class LinearAging(AgingModelInterface):
    def __init__(
        self,
        aging_rate_ppb_per_day: float,
        initial_age_ppb: float = 0,
        initial_age_time_seconds: float = 0,
    ):
        super().__init__()

        self.__aging_rate_ppb_per_day = aging_rate_ppb_per_day
        self.__aging_rate_ppb_per_second = self.__aging_rate_ppb_per_day / (3600 * 24)
        self.__initial_age_ppb = initial_age_ppb
        self.__initial_age_time_seconds = initial_age_time_seconds

    def generate(self, reference_time_seconds: numpy.array) -> numpy.array:
        offset_time_seconds = reference_time_seconds - self.__initial_age_time_seconds

        ffo_ppb = (
            offset_time_seconds * self.__aging_rate_ppb_per_second
        ) + self.__initial_age_ppb

        return ffo_ppb
