#
# Copyright 2017 Russell Smiley
#
# This file is part of timetools.
#
# timetools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# timetools is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with timetools.  If not, see <http://www.gnu.org/licenses/>.
#

import typing

import numpy

from .oscillator import OscillatorModel


def calculate_local_time_from_ffo(
    reference_time_seconds,
    lo_ffo_ppb,
    initial_ffo_ppb: float = 0,
    initial_reference_time_seconds: float = 0,
    initial_local_time_seconds: float = 0,
):
    assert lo_ffo_ppb.shape == reference_time_seconds.shape

    # Make sure that the FFO from a previous iteration is accurately accounted for.
    initial_ffo_array = numpy.array([initial_ffo_ppb])
    if lo_ffo_ppb != []:
        instantaneous_ffo_ppb = numpy.concatenate(
            (initial_ffo_array, lo_ffo_ppb[: (len(lo_ffo_ppb) - 1)])
        )
    else:
        instantaneous_ffo_ppb = initial_ffo_array

    time_delta = numpy.diff(
        numpy.concatenate(
            (numpy.array([initial_reference_time_seconds]), reference_time_seconds)
        )
    )

    assert instantaneous_ffo_ppb.shape == time_delta.shape

    time_change = (time_delta * instantaneous_ffo_ppb * 1e-9) + time_delta
    local_time_seconds = numpy.array([initial_local_time_seconds]) + numpy.cumsum(
        time_change
    )

    assert local_time_seconds.shape == reference_time_seconds.shape

    return local_time_seconds


class ClockModel:
    """
        A simple clock model with initial static offset and optional Gaussian frequency
        jitter. The initial time offset is assumed to be zero.

        The frequency offset and resulting time offset are returned by
        calculateOffset. calculateOffset is iterative so that subsequent calls to
        calculateOffset calculate the offset relative to the last call.
    """

    def __init__(
        self,
        oscillator_model: OscillatorModel,
        initial_time_offset_seconds: float = 0,
        initial_reference_time_seconds: float = 0,
        initial_reference_temperature_kelvin: float = 0,
    ):
        # Support iterative use of the model by retaining relevant data from the previous iteration.
        self.last_reference_time_seconds = initial_reference_time_seconds
        self.last_reference_temperature_kelvin = initial_reference_temperature_kelvin
        self.last_local_time_offset_seconds = initial_time_offset_seconds
        self.last_instantaneous_ffo_ppb = oscillator_model.initial_ffo_ppb

        self.oscillator_model = oscillator_model

    def generate(
        self,
        reference_time_seconds: numpy.array,
        reference_temperature_kelvin: typing.Optional[numpy.array] = None,
    ) -> typing.Tuple[numpy.array, numpy.array]:
        lo_ffo_ppb = self.oscillator_model.generate(
            reference_time_seconds, reference_temperature_kelvin
        )

        # If there is any time delta between self._lastReferenceTimeSeconds and referenceTimeSeconds[0]
        # then this must be accounted for in the skew and subsequent time integration calculation.
        local_time_seconds = calculate_local_time_from_ffo(
            reference_time_seconds,
            lo_ffo_ppb,
            self.last_instantaneous_ffo_ppb,
            self.last_reference_time_seconds,
            self.last_local_time_offset_seconds,
        )

        self.last_local_time_offset_seconds = local_time_seconds[-1]
        self.last_instantaneous_ffo_ppb = lo_ffo_ppb[-1]
        # Make this a numpy array even though it is only a single scalar so that numpy concatenation works for
        # timeDelta calculation.
        self.last_reference_time_seconds = reference_time_seconds[-1]
        if reference_temperature_kelvin is not None:
            self.last_reference_temperature_kelvin = reference_temperature_kelvin[-1]

        return local_time_seconds, lo_ffo_ppb
