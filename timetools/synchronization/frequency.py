#
# Copyright 2017 Russell Smiley
#
# This file is part of timetools.
#
# timetools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# timetools is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with timetools.  If not, see <http://www.gnu.org/licenses/>.
#

import typing

import numpy

FrequencyDataTypes = typing.Union[numpy.array, float]


def convert_skew_to_ffo_ppb(skew: FrequencyDataTypes) -> FrequencyDataTypes:
    """
    Convert frequency skew to Fraction Frequency Offset (FFO) in ppb.

    :param skew: Numpy array or float of skew.

    :return: Numpy array or float of FFO in ppb.
    """
    return (skew - 1) / 1e-9


def convert_ffo_ppb_to_skew(ffo_ppb: FrequencyDataTypes) -> FrequencyDataTypes:
    """
    Convert frequency skew to Fraction Frequency Offset (FFO) in ppb.

    :param ffo_ppb: Numpy array or float of FFO in ppb.

    :return: Numpy array or float of skew.
    """
    return (ffo_ppb * 1e-9) + 1


def convert_ffo_ppb_to_offset_hz(
    ffoPpb: FrequencyDataTypes, referenceFrequencyHz: FrequencyDataTypes
) -> FrequencyDataTypes:
    """
    Convert Fractional Frequency Offset (FFO) in ppb to FFO in Hz.

    The reference frequency in Hz also needs to be specified.

    :param ffoPpb: Numpy array or float of FFO in ppb.
    :param referenceFrequencyHz: Reference frequency in Hz.

    :return: Numpy array or float of frequency in Hz.
    """

    return ffoPpb * referenceFrequencyHz * 1e-9


def convert_skew_to_offset_hz(
    skew: FrequencyDataTypes, reference_frequency_hz: FrequencyDataTypes
) -> FrequencyDataTypes:
    """
    Convert Fractional Frequency Offset (FFO) in ppb to FFO in Hz.

    The reference frequency in Hz also needs to be specified.

    :param skew: Numpy array or float of skew.
    :param reference_frequency_hz: Reference frequency in Hz.

    :return: Numpy array or float of frequency in Hz.
    """

    return (skew * reference_frequency_hz) - reference_frequency_hz


def convert_ffo_ppb_to_hz(
    ffo_ppb: FrequencyDataTypes, reference_frequency_hz: FrequencyDataTypes
) -> FrequencyDataTypes:
    """
    Convert Fractional Frequency Offset (FFO) in ppb to absolute frequency in Hz.

    The reference frequency in Hz also needs to be specified.

    :param ffo_ppb: Numpy array or float of FFO in ppb.
    :param reference_frequency_hz: Reference frequency in Hz.

    :return: Numpy array or float of frequency in Hz.
    """

    return (ffo_ppb * reference_frequency_hz * 1e-9) + reference_frequency_hz


def convert_skew_to_hz(
    skew: FrequencyDataTypes, reference_frequency_hz: FrequencyDataTypes
) -> FrequencyDataTypes:
    """
    Convert Fractional Frequency Offset (FFO) in ppb to absolute frequency in Hz.

    The reference frequency in Hz also needs to be specified.

    :param skew: Numpy array or float of skew.
    :param reference_frequency_hz: Reference frequency in Hz.

    :return: Numpy array or float of frequency in Hz.
    """

    return skew * reference_frequency_hz
