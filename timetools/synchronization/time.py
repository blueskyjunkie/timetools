#
# Copyright 2017 Russell Smiley
#
# This file is part of timetools.
#
# timetools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# timetools is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with timetools.  If not, see <http://www.gnu.org/licenses/>.
#

import math

import numpy


class ReferenceGenerator:
    """
    Generate a uniformly sampled timebase, iteratively as necessary, but of arbitrary size each cycle.
    """

    def __init__(self, time_step: float, initial_time_offset: float = 0):
        self.__time_step = time_step
        # This calculation assumes uniform sampling
        self.__last_iteration_end_index = math.floor(
            initial_time_offset / self.__time_step
        )

    def generate(self, number_samples: int = 1) -> numpy.array:
        this_iteration_indices = self.__last_iteration_end_index + (
            numpy.arange(0, number_samples)
        )
        iteration_reference_time_seconds = this_iteration_indices * self.__time_step

        self.__last_iteration_end_index = this_iteration_indices[-1] + 1

        return iteration_reference_time_seconds
