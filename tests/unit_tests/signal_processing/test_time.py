#
# Copyright 2020 Russell Smiley
#
# This file is part of timetools.
#
# timetools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# timetools is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with timetools.  If not, see <http://www.gnu.org/licenses/>.
#

# DO NOT DELETE! Used by mocker.patch below
import timetools.signal_processing.time
from timetools.signal_processing.time import StopWatch, print_time


class TestPrintTime:
    def test_hours(self):
        elapsed_time_seconds = 3600

        actual_text = print_time(elapsed_time_seconds)

        assert actual_text == "1 hrs, 0.00 s"

    def test_minutes(self):
        elapsed_time_seconds = 1800

        actual_text = print_time(elapsed_time_seconds)

        assert actual_text == "30 min, 0.00 s"

    def test_seconds1(self):
        elapsed_time_seconds = 45

        actual_text = print_time(elapsed_time_seconds)

        assert actual_text == "45.00 s"

    def test_seconds2(self):
        elapsed_time_seconds = 90

        actual_text = print_time(elapsed_time_seconds)

        assert actual_text == "1 min, 30.00 s"

    def test_seconds2(self):
        elapsed_time_seconds = 90.314

        actual_text = print_time(elapsed_time_seconds)

        assert actual_text == "1 min, 30.31 s"


class TestStopWatch:
    def test_default(self, mocker):
        mocker.patch("timetools.signal_processing.time.time.time", side_effect=[1])

        under_test = StopWatch()

        assert under_test.report_timers() == [1]

    def test_record_timer(self, mocker):
        mocker.patch("timetools.signal_processing.time.time.time", side_effect=[1, 2])

        under_test = StopWatch()
        under_test.record_timer()

        assert under_test.report_timers() == [1, 2]

    def test_show_elapsed(self, mocker):
        mocker.patch(
            "timetools.signal_processing.time.time.time", side_effect=[1, 3610]
        )

        under_test = StopWatch()
        actual_value = under_test.show_elapsed("The elapsed time is")

        assert actual_value == "The elapsed time is 1 hrs, 9.00 s"

    def test_show_total(self, mocker):
        mocker.patch(
            "timetools.signal_processing.time.time.time", side_effect=[1, 3610]
        )

        under_test = StopWatch()
        actual_value = under_test.show_total("The total time is")

        assert actual_value == "The total time is 1 hrs, 9.00 s"
