#
# Copyright 2017 Russell Smiley
#
# This file is part of timetools.
#
# timetools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# timetools is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with timetools.  If not, see <http://www.gnu.org/licenses/>.
#

import numpy
import pytest

from timetools.signal_processing.tolerance import (
    ToleranceException,
    ToleranceUnit,
    ToleranceValue,
)


class TestTolerance:
    def test_tolerance_percent1(self):
        actual_value = 10.3

        expected_value = 10
        tolerance_value = 5
        this_tolerance_unit = ToleranceUnit["percent"]
        under_test = ToleranceValue(
            expected_value, tolerance_value, this_tolerance_unit
        )

        result = under_test.isWithinTolerance(actual_value)

        assert result

    def test_tolerance_percent2(self):
        actual_value = 9.7

        expected_value = 10
        this_tolerance_unit = ToleranceUnit["percent"]

        tolerance_value1 = 5
        under_test1 = ToleranceValue(
            expected_value, tolerance_value1, this_tolerance_unit
        )

        result1 = under_test1.isWithinTolerance(actual_value)

        assert result1

        tolerance_value2 = 2
        under_test2 = ToleranceValue(
            expected_value, tolerance_value2, this_tolerance_unit
        )

        result2 = under_test2.isWithinTolerance(actual_value)

        assert not result2

    def test_tolerance_percent3(self):
        actual_value = numpy.array([9.7, 10.1])

        expected_value = 10
        tolerance_value = [4, 2]
        this_tolerance_unit = ToleranceUnit["percent"]
        under_test = ToleranceValue(
            expected_value, tolerance_value, this_tolerance_unit
        )

        result = under_test.isWithinTolerance(actual_value)

        assert result

    def under_testPercent4(self):
        actual_value = -10.3

        expected_value = -10
        this_tolerance_unit = ToleranceUnit["percent"]

        tolerance_value1 = 5
        under_test1 = ToleranceValue(
            expected_value, tolerance_value1, this_tolerance_unit
        )

        result1 = under_test1.isWithinTolerance(actual_value)

        assert result1

        tolerance_value2 = 2
        under_test2 = ToleranceValue(
            expected_value, tolerance_value2, this_tolerance_unit
        )

        result2 = under_test2.isWithinTolerance(actual_value)

        assert not result2

    def under_testAbsolute1(self):
        actual_value = numpy.array([9.7, 10.1])

        expected_value = 10
        tolerance_value = 9.65
        this_tolerance_unit = ToleranceUnit["absolute"]

        with pytest.raises(ToleranceException):
            under_test = ToleranceValue(
                expected_value, tolerance_value, this_tolerance_unit
            )

            under_test.isWithinTolerance(actual_value)

    def under_testAbsolute2(self):
        actual_value = numpy.array([9.7, 10.1])

        expected_value = 10
        tolerance_value = [9.65, 10.15]
        this_tolerance_unit = ToleranceUnit["absolute"]
        under_test = ToleranceValue(
            expected_value, tolerance_value, this_tolerance_unit
        )

        result = under_test.isWithinTolerance(actual_value)

        assert result

    def under_testRelative1(self):
        actual_value = numpy.array([9.7, 10.2])

        expected_value = 10
        this_tolerance_unit = ToleranceUnit["relative"]

        tolerance_value1 = 0.4
        under_test1 = ToleranceValue(
            expected_value, tolerance_value1, this_tolerance_unit
        )

        result1 = under_test1.isWithinTolerance(actual_value)

        assert result1

        tolerance_value2 = 0.1
        under_test2 = ToleranceValue(
            expected_value, tolerance_value2, this_tolerance_unit
        )

        result2 = under_test2.isWithinTolerance(actual_value)

        assert not result2

    def under_testRelative2(self):
        actual_value = numpy.array([9.7, 10.1])

        expected_value = 10
        tolerance_value = [0.4, 0.2]
        this_tolerance_unit = ToleranceUnit["relative"]
        under_test = ToleranceValue(
            expected_value, tolerance_value, this_tolerance_unit
        )

        result = under_test.isWithinTolerance(actual_value)

        assert result

    def under_testRelative3(self):
        actual_value = numpy.array([-9.7, -10.1])

        expected_value = -10
        this_tolerance_unit = ToleranceUnit["relative"]

        tolerance_value1 = [0.2, 0.4]
        under_test1 = ToleranceValue(
            expected_value, tolerance_value1, this_tolerance_unit
        )

        result1 = under_test1.isWithinTolerance(actual_value)

        assert result1

        tolerance_value2 = [0.4, 0.2]
        under_test2 = ToleranceValue(
            expected_value, tolerance_value2, this_tolerance_unit
        )

        result2 = under_test2.isWithinTolerance(actual_value)

        assert not result2
