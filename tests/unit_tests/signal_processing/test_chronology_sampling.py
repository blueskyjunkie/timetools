#
# Copyright 2017 Russell Smiley
#
# This file is part of timetools.
#
# timetools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# timetools is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with timetools.  If not, see <http://www.gnu.org/licenses/>.
#

import numpy

from timetools.signal_processing.chronology import Chronology, decimate


class TestChronologyDecimation:
    def test_decimate(self):
        timebase = numpy.arange(0, 10)
        signal = numpy.arange(0, 10) + 5

        input_chronology = Chronology(timebase, signal)

        expected_timebase = numpy.array([0, 2, 4, 6, 8])
        expected_signal = numpy.array([5, 7, 9, 11, 13])

        output_chronology = decimate(input_chronology, 2)

        assert numpy.all(expected_timebase == output_chronology.timebase)
        assert numpy.all(expected_signal == output_chronology.signal)
