#
# Copyright 2020 Russell Smiley
#
# This file is part of timetools.
#
# timetools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# timetools is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with timetools.  If not, see <http://www.gnu.org/licenses/>.
#

import numpy

from timetools.signal_processing.autocorrelation import autocorrelation


class TestAutocorrelation:
    def test_even_length(self):
        a1 = numpy.array([0, 1, 0, 0])

        actual = autocorrelation(a1)

        expected = numpy.array([1, 0, 0, 0])

        assert numpy.all(actual == expected)

    def test_odd_length(self):
        a1 = numpy.array([0, 1, 0, 0, 0])

        actual = autocorrelation(a1)

        expected = numpy.array([1, 0, 0, 0, 0])

        assert numpy.all(actual == expected)
