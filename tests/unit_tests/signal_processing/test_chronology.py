#
# Copyright 2017 Russell Smiley
#
# This file is part of timetools.
#
# timetools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# timetools is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with timetools.  If not, see <http://www.gnu.org/licenses/>.
#

import numpy
import pytest

from timetools.signal_processing import SignalProcessingError
from timetools.signal_processing.chronology import Chronology


class TestChronology:
    def test_mismatched_len_raises(self):
        t1 = numpy.array([0, 1, 2])
        d1 = numpy.array([0, 10, 0, 0, 10])

        with pytest.raises(
            SignalProcessingError,
            match=r"^Chronology timebase and signals must be equal lengths",
        ):
            Chronology(t1, d1)

    def test_nonmonotonic_timebase_raises(self):
        t1 = numpy.array([0, 4, 2])
        d1 = numpy.array([0, 10, 0])

        with pytest.raises(
            SignalProcessingError,
            match=r"^Chronology timebase is not monotonically increasing",
        ):
            Chronology(t1, d1)

    def test_len(self):
        number_samples = 10
        timebase = numpy.arange(0, number_samples)
        data = numpy.random.normal(0, 1, number_samples)

        under_test = Chronology(timebase, data)

        expected_number_samples = number_samples
        actual_number_samples = len(under_test)

        assert expected_number_samples == actual_number_samples

    def test_logical_indices(self):
        """Chronology uses logical indices to only measure where the time bases are the same."""
        t1 = numpy.array([0, 1, 2, 3, 4])
        d1 = numpy.array([0, 10, 0, 0, 10])
        t2 = numpy.array([0, 2, 3, 10])
        d2 = numpy.array([1, 2, 3, 6])

        c1 = Chronology(t1, d1)
        c2 = Chronology(t2, d2)

        greater_result = c2 > c1
        less_result = c1 < c2

        assert numpy.all(greater_result.signal)
        assert numpy.all(less_result.signal)

    def test_lt(self):
        timebase = numpy.array([0, 1, 2])
        d1 = numpy.array([1, 2, 3])
        d2 = numpy.array([3, 4, 5])
        d3 = numpy.array([0, 4, 0])

        c1 = Chronology(timebase, d1)
        c2 = Chronology(timebase, d2)
        c3 = Chronology(timebase, d3)

        c1c2_less_result = c1 < c2
        c2c1_less_result = c2 < c1

        assert numpy.all(c1c2_less_result.signal)
        assert not numpy.all(c2c1_less_result.signal)

        c3c1_less_result = c3 < c1
        c1c3_less_result = c1 < c3

        assert not numpy.all(c3c1_less_result.signal)
        assert not numpy.all(c1c3_less_result.signal)

    def test_le(self):
        timebase = numpy.array([0, 1, 2])
        d1 = numpy.array([1, 4, 3])
        d2 = numpy.array([3, 4, 5])
        d3 = numpy.array([0, 5, 0])
        d4 = numpy.array([0, 4, 0])

        c1 = Chronology(timebase, d1)
        c2 = Chronology(timebase, d2)
        c3 = Chronology(timebase, d3)
        c4 = Chronology(timebase, d4)

        c1c2_le_result = c1 <= c2
        c4c1_le_result = c4 <= c1

        assert numpy.all(c1c2_le_result.signal)
        assert numpy.all(c4c1_le_result.signal)

        c2c1_le_result = c2 <= c1

        assert not numpy.all(c2c1_le_result.signal)

        c3c1_le_result = c3 <= c1
        c1c3_le_result = c1 <= c3

        assert not numpy.all(c3c1_le_result.signal)
        assert not numpy.all(c1c3_le_result.signal)
