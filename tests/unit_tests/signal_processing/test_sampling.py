#
# Copyright 2017 Russell Smiley
#
# This file is part of timetools.
#
# timetools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# timetools is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with timetools.  If not, see <http://www.gnu.org/licenses/>.
#

import numpy

from timetools.signal_processing.sampling import upsample


class TestSampling:
    def test_upsample1(self):
        upsample_ratio = 2
        x = numpy.array([1, 3, 6])
        expected_new_x = numpy.array([1, 1, 3, 3, 6, 6])

        actual_new_x = upsample(x, upsample_ratio)

        assert numpy.all(expected_new_x == actual_new_x)

    def test_upsample2(self):
        upsample_ratio = 4
        x = numpy.array([3])
        expected_new_x = numpy.array([3, 3, 3, 3])

        actual_new_x = upsample(x, upsample_ratio)

        assert numpy.all(expected_new_x == actual_new_x)

    def test_upsample3(self):
        # Specify a floating point upsampling ratio
        upsample_ratio = 4.0
        x = numpy.array([3])
        expected_new_x = numpy.array([3, 3, 3, 3])

        actual_new_x = upsample(x, upsample_ratio)

        assert numpy.all(expected_new_x == actual_new_x)
