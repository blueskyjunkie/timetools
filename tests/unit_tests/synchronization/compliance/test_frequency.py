#
# Copyright 2017 Russell Smiley
#
# This file is part of timetools.
#
# timetools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# timetools is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with timetools.  If not, see <http://www.gnu.org/licenses/>.
#

from timetools.synchronization.clock import ClockModel
from timetools.synchronization.compliance.frequency import (
    ffo16ppbMask,
    generateThresholdMask,
)
from timetools.synchronization.oscillator import OscillatorModel
from timetools.synchronization.oscillator.noise.gaussian import GaussianNoise
from timetools.synchronization.time import ReferenceGenerator


class TestFrequencyCompliance:
    def test_ffo16_ppb1(self):
        time_step_seconds = 1 / 16
        number_samples = 1000

        clock_ffo_ppb = 0
        clock_rms_jitter_ppb = 3.0

        reference_time_generator = ReferenceGenerator(time_step_seconds)
        reference_time_seconds = reference_time_generator.generate(number_samples)

        clock_model = ClockModel(
            OscillatorModel(
                initial_ffo_ppb=clock_ffo_ppb,
                noise_model=GaussianNoise(
                    standard_deviation_ppb=clock_rms_jitter_ppb, seed=1459
                ),
            )
        )

        local_time_seconds, instantaneous_lo_ffo_ppb = clock_model.generate(
            reference_time_seconds
        )

        analysis_result = ffo16ppbMask.evaluate(
            (reference_time_seconds, instantaneous_lo_ffo_ppb)
        )

        assert analysis_result

    def test_ffo16_ppb2(self):
        time_step_seconds = 1 / 16
        number_samples = 1000

        clock_ffo_ppb = 0
        clock_rms_jitter_ppb = 10

        reference_time_generator = ReferenceGenerator(time_step_seconds)
        reference_time_seconds = reference_time_generator.generate(number_samples)

        clock_model = ClockModel(
            OscillatorModel(
                initial_ffo_ppb=clock_ffo_ppb,
                noise_model=GaussianNoise(
                    standard_deviation_ppb=clock_rms_jitter_ppb, seed=1459
                ),
            )
        )

        local_time_seconds, instantaneous_lo_ffo_ppb = clock_model.generate(
            reference_time_seconds
        )

        analysis_result = ffo16ppbMask.evaluate(
            (reference_time_seconds, instantaneous_lo_ffo_ppb)
        )

        assert not analysis_result

    def test_generate_threshold_mask1(self):
        time_step_seconds = 1 / 16
        number_samples = 1000

        clock_ffo_ppb = 0
        clock_rms_jitter_ppb = 0.2

        compliance_threshold_ppb = 1.0

        this_mask = generateThresholdMask(compliance_threshold_ppb)

        reference_time_generator = ReferenceGenerator(time_step_seconds)
        reference_time_seconds = reference_time_generator.generate(number_samples)

        clock_model = ClockModel(
            OscillatorModel(
                initial_ffo_ppb=clock_ffo_ppb,
                noise_model=GaussianNoise(
                    standard_deviation_ppb=clock_rms_jitter_ppb, seed=1459
                ),
            )
        )

        local_time_seconds, instantaneous_lo_ffo_ppb = clock_model.generate(
            reference_time_seconds
        )

        analysis_result = this_mask.evaluate(
            (reference_time_seconds, instantaneous_lo_ffo_ppb)
        )

        assert analysis_result

    def test_generate_threshold_mask2(self):
        time_step_seconds = 1 / 16
        number_samples = 1000

        clock_ffo_ppb = 0
        clock_rms_jitter_ppb = 3.0

        compliance_threshold_ppb = 1.0

        this_mask = generateThresholdMask(compliance_threshold_ppb)

        reference_time_generator = ReferenceGenerator(time_step_seconds)
        reference_time_seconds = reference_time_generator.generate(number_samples)

        clock_model = ClockModel(
            OscillatorModel(
                initial_ffo_ppb=clock_ffo_ppb,
                noise_model=GaussianNoise(
                    standard_deviation_ppb=clock_rms_jitter_ppb, seed=1459
                ),
            )
        )

        local_time_seconds, instantaneous_lo_ffo_ppb = clock_model.generate(
            reference_time_seconds
        )

        analysis_result = this_mask.evaluate(
            (reference_time_seconds, instantaneous_lo_ffo_ppb)
        )

        assert not analysis_result
