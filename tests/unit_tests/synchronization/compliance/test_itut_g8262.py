#
# Copyright 2017 Russell Smiley
#
# This file is part of timetools.
#
# timetools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# timetools is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with timetools.  If not, see <http://www.gnu.org/licenses/>.
#

from timetools.synchronization.compliance import (
    g8262_eec1_frequency_accuracy,
    g8262_eec1_holdover,
    g8262_eec1_wander_generation,
    g8262_eec1_wander_tolerance,
    g8262_eec2_frequency_accuracy,
    g8262_eec2_holdover,
    g8262_eec2_noise_transfer,
    g8262_eec2_phase_discontinuity,
    g8262_eec2_transient,
    g8262_eec2_wander_generation,
    g8262_eec2_wander_tolerance,
    g8262_eec2_wander_transfer,
    g8262_jitter_generation,
    g8262_jitter_tolerance,
)


class TestItuTG8262:
    def test_eec1_mtie_constant_temperature_g8262_wander_generation_mask(self):
        this_mask = g8262_eec1_wander_generation.mtie_constant_temperature_ns

    def test_eec1_mtie_variable_temperature_g8262_wander_generation_mask(self):
        this_mask = g8262_eec1_wander_generation.mtie_temperature_ns

    def test_eec1_tdev_constant_temperature_g8262_wander_generation_mask(self):
        this_mask = g8262_eec1_wander_generation.tdev_constant_temperature_ns

    def test_eec1_mtie_constant_temperature_g8262_wander_tolerance_mask(self):
        this_mask = g8262_eec1_wander_tolerance.mtie_mask_microseconds

    def test_eec1_tdev_constant_temperature_g8262_wander_tolerance_mask(self):
        this_mask = g8262_eec1_wander_tolerance.tdev_mask_ns

    def test_eec2_mtie_constant_temperature_g8262_wander_generation_mask(self):
        this_mask = g8262_eec2_wander_generation.mtie_constant_temperature_ns

    def test_eec2_tdev_constant_temperature_g8262_wander_generation_mask(self):
        this_mask = g8262_eec2_wander_generation.tdev_constant_temperature_ns

    def test_eec2_tdev_g8262_wander_tolerance_mask(self):
        this_mask = g8262_eec2_wander_tolerance.tdev_ns

    def test_g8262_jitter_tolerance1_g_mask(self):
        this_mask = g8262_jitter_tolerance.jitter_amplitude_1g

    def test_g8262_jitter_tolerance10_g_mask(self):
        this_mask = g8262_jitter_tolerance.jitter_amplitude_10g

    def test_eec2_tdev_g8262_wander_transfer_mask(self):
        this_mask = g8262_eec2_wander_transfer.tdev_ns

    def test_eec2_mtie_transient_mask(self):
        this_mask = g8262_eec2_transient.transient_mtie_ns

    def test_eec1_holdover_phase_error_mask(self):
        this_mask = g8262_eec1_holdover.phase_error_ns

    def test_eec2_holdover_phase_error_mask(self):
        this_mask = g8262_eec2_holdover.phase_error_ns

    def test_eec1_sinusoidal_wander_mask(self):
        this_mask = g8262_eec1_wander_tolerance.generate_sinusoidal_mask()

    def test_eec2_tdev_noise_transfer_mask(self):
        this_mask = g8262_eec2_noise_transfer.tdev_ns

    def test_eec2_frequency_accuracy_mask(self):
        this_mask = g8262_eec2_frequency_accuracy.ffo_ppm

    def test_eec1_frequency_accuracy_mask(self):
        this_mask = g8262_eec1_frequency_accuracy.generate_ffo_mask_ppm()

    def test_eec2_tdev_phase_discontinuity_mask(self):
        this_mask = g8262_eec2_phase_discontinuity.mtie_ns

    def test_jitter_generation1_g_mask(self):
        this_mask = g8262_jitter_generation.amplitude_1g

    def test_jitter_generation10_g_mask(self):
        this_mask = g8262_jitter_generation.amplitude_10g
