#
# Copyright 2017 Russell Smiley
#
# This file is part of timetools.
#
# timetools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# timetools is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with timetools.  If not, see <http://www.gnu.org/licenses/>.
#

from timetools.synchronization.analysis.itut_g810 import calculate_time_error
from timetools.synchronization.clock import ClockModel
from timetools.synchronization.compliance.time import time1usMask
from timetools.synchronization.oscillator import OscillatorModel
from timetools.synchronization.oscillator.noise.gaussian import GaussianNoise
from timetools.synchronization.time import ReferenceGenerator


class TestComplianceTime:
    def test_compliance_time1(self):
        time_step_seconds = 1 / 16
        number_samples = 2000

        clock_ffo_ppb = 0
        clock_rms_jitter_ppb = 3.0

        reference_time_generator = ReferenceGenerator(time_step_seconds)
        reference_time_seconds = reference_time_generator.generate(number_samples)

        clock_model = ClockModel(
            OscillatorModel(
                initial_ffo_ppb=clock_ffo_ppb,
                noise_model=GaussianNoise(
                    standard_deviation_ppb=clock_rms_jitter_ppb, seed=1459
                ),
            )
        )
        local_time_seconds, instantaneous_lo_ffo_ppb = clock_model.generate(
            reference_time_seconds
        )

        time_error_microseconds = (
            calculate_time_error(local_time_seconds, reference_time_seconds) / 1e-6
        )

        analysis_result = time1usMask.evaluate(
            (reference_time_seconds, time_error_microseconds)
        )

        assert analysis_result

    def test_compliance_time2(self):
        time_step_seconds = 1 / 16
        number_samples = 2000

        clock_ffo_ppb = 10
        clock_rms_jitter_ppb = 3.0

        reference_time_generator = ReferenceGenerator(time_step_seconds)
        reference_time_seconds = reference_time_generator.generate(number_samples)

        clock_model = ClockModel(
            OscillatorModel(
                initial_ffo_ppb=clock_ffo_ppb,
                noise_model=GaussianNoise(
                    standard_deviation_ppb=clock_rms_jitter_ppb, seed=1459
                ),
            )
        )
        local_time_seconds, instantaneous_lo_ffo_ppb = clock_model.generate(
            reference_time_seconds
        )

        time_error_microseconds = (
            calculate_time_error(local_time_seconds, reference_time_seconds) / 1e-6
        )

        analysis_result = time1usMask.evaluate(
            (reference_time_seconds, time_error_microseconds)
        )

        assert not analysis_result
