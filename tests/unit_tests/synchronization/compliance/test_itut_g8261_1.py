#
# Copyright 2017 Russell Smiley
#
# This file is part of timetools.
#
# timetools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# timetools is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with timetools.  If not, see <http://www.gnu.org/licenses/>.
#

import random

import numpy

from timetools.synchronization.compliance.itut_g8261_1.pdv import (
    calculate_cluster_peak,
    calculate_floor_packet_percent,
    calculate_result_size,
    gather_results,
    scatter_indices,
)


class TestItuTG82611:
    def _generate_split_result(self, split_indices, results):
        # Assume results is a numeric array
        split_results = []
        for this_group in split_indices:
            split_results.append((this_group, results[this_group]))

        return split_results

    def _generate_split_result2(self, split_indices, results):
        # Assume results is an array of arbitrary type elements
        split_results = []
        for this_group in split_indices:
            result_group = []
            for this_index in this_group:
                result_group.append(results[this_index])

            split_results.append((this_group, result_group))

        return split_results

    def test_calculate_floor_packet_percent(self):
        threshold = 5
        expected = 6

        pdv = numpy.arange(0, 100, 1)
        result = calculate_floor_packet_percent(pdv, threshold)

        assert result == expected

    def test_calculate_cluster_peak(self):
        threshold = 4
        expected = 4

        pdv = numpy.arange(0, 100, 1)
        result = calculate_cluster_peak(pdv, threshold)

        assert result == expected

    def test_scatter_indices(self):
        number_groups = 4
        indices = numpy.arange(0, 9)

        split_indices = scatter_indices(indices, number_groups)

        assert len(split_indices) == number_groups

    def test_calculate_result_size(self):
        number_groups = 4
        indices = numpy.arange(0, 9)
        results = indices + 14

        expected_result_size = len(indices)

        split_indices = scatter_indices(indices, number_groups)

        split_result = self._generate_split_result(split_indices, results)

        actual_result_size = calculate_result_size(split_result)

        assert actual_result_size == expected_result_size

    def test_scatter_gather_indices1(self):
        number_groups = 4
        indices = numpy.arange(0, 9)
        expected_results = indices + 14

        split_indices = scatter_indices(indices, number_groups)

        split_results = self._generate_split_result(split_indices, expected_results)

        (actual_results, ordered_index_group_array) = gather_results(split_results)

        assert numpy.all(expected_results == actual_results)

    def test_scatter_gather_indices2(self):
        number_groups = 4
        expected_indices = numpy.arange(0, 9)
        expected_results = expected_indices + 14

        indices = random.sample(range(0, len(expected_indices)), len(expected_indices))

        split_indices = scatter_indices(indices, number_groups)

        split_results = self._generate_split_result(split_indices, expected_results)

        (actual_results, ordered_index) = gather_results(split_results)

        assert numpy.all(expected_results == actual_results)
        assert numpy.all(expected_indices == ordered_index)

    def test_scatter_gather_indices3(self):
        number_groups = 4
        indices = numpy.arange(0, 9)
        expected_results1 = indices + 14
        expected_results2 = indices + 32

        expected_results = [
            (x, y) for x, y in zip(expected_results1, expected_results2)
        ]

        split_indices = scatter_indices(indices, number_groups)
        split_results = self._generate_split_result2(split_indices, expected_results)

        (actual_results, ordered_index_group_array) = gather_results(split_results)

        actual_result1 = numpy.array([x[0] for x in actual_results])
        actual_result2 = numpy.array([x[1] for x in actual_results])

        assert numpy.all(expected_results1 == actual_result1)
        assert numpy.all(expected_results2 == actual_result2)
