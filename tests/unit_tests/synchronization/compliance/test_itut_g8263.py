#
# Copyright 2017 Russell Smiley
#
# This file is part of timetools.
#
# timetools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# timetools is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with timetools.  If not, see <http://www.gnu.org/licenses/>.
#

import typing
from collections import namedtuple

import numpy
import pytest

import timetools.synchronization.compliance.itut_g8263.holdover_transient as g8263_holdover_transient
import timetools.synchronization.compliance.itut_g8263.wander_generation as g8263_wander_generation
from timetools.synchronization.clock import ClockModel
from timetools.synchronization.compliance.itut_g8263.compute import (
    analyze_itut_g8263_mask,
)
from timetools.synchronization.oscillator import OscillatorModel
from timetools.synchronization.oscillator.noise import GaussianNoise
from timetools.synchronization.time import ReferenceGenerator

SignalConfiguration = namedtuple(
    "SignalConfiguration", ["time_step_seconds", "number_samples", "random_seed"]
)


@pytest.fixture
def signal_config() -> SignalConfiguration:
    time_step_seconds = 1
    number_samples = 10000
    seed = 1459

    signal_configuration = SignalConfiguration(
        time_step_seconds=time_step_seconds,
        number_samples=number_samples,
        random_seed=seed,
    )
    return signal_configuration


def generate_clock_response(
    clock_ffo_ppb: float,
    clock_rms_jitter_ppb: float,
    signal_configuration: SignalConfiguration,
) -> typing.Tuple[numpy.array, numpy.array]:

    reference_time_generator = ReferenceGenerator(
        signal_configuration.time_step_seconds
    )
    reference_time_seconds = reference_time_generator.generate(
        signal_configuration.number_samples
    )

    clock_model = ClockModel(
        OscillatorModel(
            initial_ffo_ppb=clock_ffo_ppb,
            noise_model=GaussianNoise(
                standard_deviation_ppb=clock_rms_jitter_ppb,
                seed=signal_configuration.random_seed,
            ),
        )
    )
    local_time_seconds, instantaneous_lo_ffo_ppb = clock_model.generate(
        reference_time_seconds
    )

    return (local_time_seconds, reference_time_seconds)


class TestItuTG8263:
    def test_constant_temperature_wander_generation_mask(self):
        this_mask = g8263_wander_generation.constant_temperature_mtie_mask_ns

    def test_variable_temperature_wander_generation_mask(self):
        constant_temperature_mask = (
            g8263_wander_generation.constant_temperature_mtie_mask_ns
        )
        this_mask = g8263_wander_generation.variable_temperature_mtie_mask_ns

    def test_holdover_transient_phase_error_mask(self):
        this_mask = g8263_holdover_transient.phase_error_mask_ns

    def test_holdover_transient_ffo_mask(self):
        this_mask = g8263_holdover_transient.ffo_mask_ppb

    def test_holdover_transient_ffo_rate_mask(self):
        this_mask = g8263_holdover_transient.ffo_rate_mask_ppb_per_second

    def test_wander_generation_constant_temperature_ns1(
        self, signal_config: SignalConfiguration
    ):
        """A clock signal small enough to pass the analysis does pass the analysis."""
        desired_number_observations = 15

        clock_ffo_ppb = 0.5
        clock_rms_jitter_ppb = 2

        local_time_seconds, reference_time_seconds = generate_clock_response(
            clock_ffo_ppb, clock_rms_jitter_ppb, signal_config
        )

        analysis_result, this_mask, mtie_data = analyze_itut_g8263_mask(
            local_time_seconds,
            reference_time_seconds,
            signal_config.time_step_seconds,
            desired_number_observations,
        )

        assert analysis_result

    def test_wander_generation_constant_temperature_ns2(
        self, signal_config: SignalConfiguration
    ):
        """A clock FFO large enough to fail the mask analysis does fail the analysis."""
        desired_number_observations = 15

        clock_ffo_ppb = 5
        clock_rms_jitter_ppb = 2

        local_time_seconds, reference_time_seconds = generate_clock_response(
            clock_ffo_ppb, clock_rms_jitter_ppb, signal_config
        )

        analysis_result, this_mask, mtie_data = analyze_itut_g8263_mask(
            local_time_seconds,
            reference_time_seconds,
            signal_config.time_step_seconds,
            desired_number_observations,
        )

        assert not analysis_result
