#
# Copyright 2017 Russell Smiley
#
# This file is part of timetools.
#
# timetools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# timetools is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with timetools.  If not, see <http://www.gnu.org/licenses/>.
#

from timetools.synchronization.compliance import (
    g8261_dc1_wander_budget,
    g8261_dc2_wander_budget,
    g8261_eec1_network_wander,
    g8261_eec2_network_wander,
)


class TestItuTG8261:
    def test_eec_option1_mtie_mask(self):
        this_mask = g8261_eec1_network_wander.mtie_ns

    def test_eec_option1_tdev_mask(self):
        this_mask = g8261_eec1_network_wander.tdev_ns

    def test_eec_option2_tdev_mask(self):
        this_mask = g8261_eec2_network_wander.tdev_ns

    def test_dc2_a2048_mrtie_mask(self):
        this_mask = g8261_dc2_wander_budget.case2_a2048_mrtie_microseconds

    def test_dc1_11544_mrtie_mask(self):
        this_mask = g8261_dc1_wander_budget.case11544_mrtie_microseconds

    def test_dc1_12048_mrtie_mask(self):
        this_mask = g8261_dc1_wander_budget.case12048_mrtie_microseconds
