#
# Copyright 2017 Russell Smiley
#
# This file is part of timetools.
#
# timetools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# timetools is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with timetools.  If not, see <http://www.gnu.org/licenses/>.
#

import matplotlib.pyplot as mpp
import numpy

from timetools.synchronization.compliance.analysis import Mask


class TestAnalysis:
    def test_mask0(self):
        # There should be no exceptions from this call
        this_mask = Mask([])

    def test_mask1(self):
        this_mask = Mask([([0.0], [10.0])])

        # The signal exceeds the mask bound so the evaluation should fail
        signal = (numpy.arange(0.0, 12.0), numpy.arange(0.0, 12.0))

        assert not this_mask.evaluate(signal)

    def test_mask2(self):
        this_mask = Mask([([0.0], [12.0])])

        # The signal is equal to the mask bounds so the evaluation should pass
        signal = (numpy.arange(0.0, 12.0), numpy.arange(0.0, 12.0))

        assert this_mask.evaluate(signal)

    def test_mask3(self):
        this_mask = Mask([([0.0, 10.0], [10.0])])

        # The signal is <= the mask bound for the specified interval so the evaluation should pass
        signal = (numpy.arange(0.0, 12.0), numpy.arange(0.0, 12.0))

        assert this_mask.evaluate(signal)

    def test_mask4(self):
        this_mask = Mask([([0.0, 10.0], [10.0], [-1.0])])

        # The signal >= the mask bound on the lower bound side and <= the mask bound on the
        # upper bound side over the mask interval so the evaluation should pass
        signal = (numpy.arange(0.0, 12.0), numpy.arange(0.0, 12.0))

        assert this_mask.evaluate(signal)

    def test_mask5(self):
        this_mask = Mask([([0.0, 10.0], ([10.0], [0.5]))])

        # The signal >= the mask bound on the lower bound side and <= the mask bound on the
        # upper bound side over the mask interval so the evaluation should pass
        signal = (numpy.arange(0.0, 12.0), numpy.arange(0.0, 12.0))

        assert this_mask.evaluate(signal)

    def test_mask6(self):
        this_mask = Mask([([0.0, 10.0], ([10.0], [0.5]))])

        # The signal >= the mask bound on the upper bound side so the evaluation should fail
        signal = (numpy.arange(0.0, 12.0), numpy.arange(0.0, 12.0) + 10)

        assert not this_mask.evaluate(signal)

    def test_mask7(self):
        this_mask = Mask([([0.0, 10.0], ([5, 10.0], [0, 0.5]))])

        # The signal >= the mask bound on the upper bound side so the evaluation should fail
        signal = (numpy.arange(0.0, 12.0), numpy.arange(0.0, 12.0) + 10)

        assert not this_mask.evaluate(signal)
