#
# Copyright 2017 Russell Smiley
#
# This file is part of timetools.
#
# timetools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# timetools is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with timetools.  If not, see <http://www.gnu.org/licenses/>.
#

import typing
from collections import namedtuple

import numpy
import pytest

import timetools.synchronization.oscillator as tso
from timetools.signal_processing.tolerance import ToleranceUnit, ToleranceValue
from timetools.synchronization.analysis.fast_tdev import (
    calculate_tdev as fast_tdev_calculation,
)
from timetools.synchronization.analysis.itut_g810 import (
    calculate_tdev as direct_tdev_calculation,
)
from timetools.synchronization.analysis.itut_g810 import calculate_tvar
from timetools.synchronization.clock import ClockModel
from timetools.synchronization.oscillator.noise import GaussianNoise
from timetools.synchronization.time import ReferenceGenerator

TestConfiguration = namedtuple(
    "TestConfiguration",
    ["local_time_seconds", "time_step_seconds", "reference_time_seconds"],
)


@pytest.fixture
def simple_setup() -> TestConfiguration:
    time_step_seconds = 1 / 16
    reference_time_seconds = numpy.arange(0, 9)
    time_error = numpy.array([1, 0, 4, 2, 5, 1, 4, 0, 3])

    local_time_seconds = reference_time_seconds + time_error

    configuration = TestConfiguration(
        local_time_seconds=local_time_seconds,
        time_step_seconds=time_step_seconds,
        reference_time_seconds=reference_time_seconds,
    )
    return configuration


def calculate_direct_tdev(
    number_observations: int, configuration: TestConfiguration
) -> typing.Tuple[numpy.array, numpy.array]:
    direct_tdev, direct_observation_intervals = direct_tdev_calculation(
        configuration.local_time_seconds,
        configuration.reference_time_seconds,
        configuration.time_step_seconds,
        number_observations,
    )

    return direct_tdev, direct_observation_intervals


@pytest.fixture
def clock_setup() -> TestConfiguration:
    time_step_seconds = 1 / 16
    number_samples = 1000

    clock_ffo_ppb = 16
    clock_rms_jitter_ppb = 3

    reference_time_generator = ReferenceGenerator(time_step_seconds)
    reference_time_seconds = reference_time_generator.generate(number_samples)

    clock_model = ClockModel(
        tso.OscillatorModel(
            initial_ffo_ppb=clock_ffo_ppb,
            noise_model=GaussianNoise(
                standard_deviation_ppb=clock_rms_jitter_ppb, seed=1459
            ),
        )
    )

    local_time_seconds, instantaneous_lo_ffo_ppb = clock_model.generate(
        reference_time_seconds
    )

    configuration = TestConfiguration(
        local_time_seconds=local_time_seconds,
        time_step_seconds=time_step_seconds,
        reference_time_seconds=reference_time_seconds,
    )
    return configuration


class TestCalculateTdev:
    def test_direct_tdev_simple(self, simple_setup: TestConfiguration):
        number_observations = 3

        expected_tdev = numpy.array([2.591193879, 0.7499999999, 0.5443310541])

        direct_tdev, direct_observation_intervals = calculate_direct_tdev(
            number_observations, simple_setup
        )

        assert len(direct_observation_intervals) == number_observations
        assert len(direct_observation_intervals) == len(direct_tdev)

        tdev_test = ToleranceValue(expected_tdev, 1e-9, ToleranceUnit["relative"])
        assert numpy.all(tdev_test.isWithinTolerance(direct_tdev))

    def test_single_proces_tdev_simple(self, simple_setup: TestConfiguration):
        number_observations = 3

        expected_tdev = numpy.array([2.591193879, 0.7499999999, 0.5443310541])

        direct_tdev, direct_observation_intervals = calculate_direct_tdev(
            number_observations, simple_setup
        )

        fast_tdev, fast_observation_intervals = fast_tdev_calculation(
            simple_setup.local_time_seconds,
            simple_setup.reference_time_seconds,
            simple_setup.time_step_seconds,
            number_observations,
            maximum_number_workers=1,
        )

        assert len(direct_tdev) == len(fast_tdev)
        assert len(direct_observation_intervals) == len(fast_observation_intervals)

        assert numpy.all(direct_observation_intervals == fast_observation_intervals)

        tdev_test1 = ToleranceValue(direct_tdev, 1e-9, ToleranceUnit["relative"])
        assert numpy.all(tdev_test1.isWithinTolerance(fast_tdev))

        tdev_test2 = ToleranceValue(expected_tdev, 1e-9, ToleranceUnit["relative"])
        assert numpy.all(tdev_test2.isWithinTolerance(fast_tdev))

    def test_single_proces_tdev_clock_model(self, clock_setup: TestConfiguration):
        number_observations = 15

        direct_tdev, direct_observation_intervals = calculate_direct_tdev(
            number_observations, clock_setup
        )

        fast_tdev, fast_observation_intervals = fast_tdev_calculation(
            clock_setup.local_time_seconds,
            clock_setup.reference_time_seconds,
            clock_setup.time_step_seconds,
            number_observations,
            maximum_number_workers=1,
        )

        assert len(direct_tdev) == len(fast_tdev)
        assert len(direct_observation_intervals) == len(fast_observation_intervals)

        assert numpy.all(direct_observation_intervals == fast_observation_intervals)

        tdev_test = ToleranceValue(direct_tdev, 1e-9, ToleranceUnit["relative"])
        assert numpy.all(tdev_test.isWithinTolerance(fast_tdev))

    def test_multiprocess_tdev_clock_model(self, clock_setup: TestConfiguration):
        number_observations = 15

        direct_tdev, direct_observation_intervals = calculate_direct_tdev(
            number_observations, clock_setup
        )

        fast_tdev, fast_observation_intervals = fast_tdev_calculation(
            clock_setup.local_time_seconds,
            clock_setup.reference_time_seconds,
            clock_setup.time_step_seconds,
            number_observations,
        )

        assert len(direct_tdev) == len(fast_tdev)
        assert len(direct_observation_intervals) == len(fast_observation_intervals)

        assert numpy.all(direct_observation_intervals == fast_observation_intervals)

        tdev_test = ToleranceValue(direct_tdev, 0.1, ToleranceUnit["percent"])
        assert numpy.all(tdev_test.isWithinTolerance(fast_tdev))


class TestCalculateTvar:
    def test_direct_tvar(self):
        time_step_seconds = 1 / 16
        reference_time_seconds = numpy.arange(0, 9)
        time_error = numpy.array([1, 0, 4, 2, 5, 1, 4, 0, 3])
        expected_tvar = numpy.power(
            numpy.array([2.591193879, 0.7499999999, 0.5443310541]), 2
        )

        number_observations = 3

        local_time_seconds = reference_time_seconds + time_error

        direct_tvar, direct_observation_intervals = calculate_tvar(
            local_time_seconds,
            reference_time_seconds,
            time_step_seconds,
            number_observations,
        )

        assert len(direct_observation_intervals) == number_observations
        assert len(direct_observation_intervals) == len(direct_tvar)

        tvar_test = ToleranceValue(expected_tvar, 1e-8, ToleranceUnit["relative"])
        assert numpy.all(tvar_test.isWithinTolerance(direct_tvar))
