#
# Copyright 2017 Russell Smiley
#
# This file is part of timetools.
#
# timetools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# timetools is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with timetools.  If not, see <http://www.gnu.org/licenses/>.
#

import time
import typing
from collections import namedtuple

import numpy
import pytest

from timetools.signal_processing.tolerance import ToleranceUnit, ToleranceValue
from timetools.synchronization.analysis.fast_mtie import (
    calculate_mtie as fast_mtie_calculation,
)
from timetools.synchronization.analysis.itut_g810 import (
    calculate_mtie as direct_mtie_calculation,
)
from timetools.synchronization.clock import ClockModel
from timetools.synchronization.oscillator import OscillatorModel
from timetools.synchronization.oscillator.noise import GaussianNoise
from timetools.synchronization.time import ReferenceGenerator

TestConfiguration = namedtuple(
    "TestConfiguration",
    ["local_time_seconds", "reference_time_seconds", "time_step_seconds"],
)


@pytest.fixture
def setup() -> TestConfiguration:
    time_step_seconds = 1 / 16
    number_samples = 10000

    clock_ffo_ppb = 16
    clock_rms_jitter_ppb = 3.0

    reference_time_generator = ReferenceGenerator(time_step_seconds)
    reference_time_seconds = reference_time_generator.generate(number_samples)

    clock_model = ClockModel(
        OscillatorModel(
            initial_ffo_ppb=clock_ffo_ppb,
            noise_model=GaussianNoise(
                standard_deviation_ppb=clock_rms_jitter_ppb, seed=1459
            ),
        )
    )

    local_time_seconds, instantaneous_lo_ffo_ppb = clock_model.generate(
        reference_time_seconds
    )

    configuration = TestConfiguration(
        local_time_seconds=local_time_seconds,
        reference_time_seconds=reference_time_seconds,
        time_step_seconds=time_step_seconds,
    )
    return configuration


def calculate_direct_mtie(
    number_observations: int, configuration: TestConfiguration
) -> typing.Tuple[numpy.array, numpy.array]:
    direct_mtie, direct_observation_intervals = direct_mtie_calculation(
        configuration.local_time_seconds,
        configuration.reference_time_seconds,
        configuration.time_step_seconds,
        number_observations,
    )

    return (direct_mtie, direct_observation_intervals)


class TestMtie:
    def test_single_process_mtie(self, setup: TestConfiguration):
        number_observations = 5

        direct_mtie, direct_observation_intervals = calculate_direct_mtie(
            number_observations, setup
        )
        # Test the single process MTIE calculation
        fast_mtie, fast_observation_intervals = fast_mtie_calculation(
            setup.local_time_seconds,
            setup.reference_time_seconds,
            setup.time_step_seconds,
            number_observations,
            maximum_number_workers=1,
        )

        assert len(direct_mtie) == len(fast_mtie)
        assert len(direct_observation_intervals) == len(fast_observation_intervals)
        assert numpy.all(
            numpy.equal(direct_observation_intervals, fast_observation_intervals)
        )

        mtie_test = ToleranceValue(direct_mtie, 0.1, ToleranceUnit["percent"])
        assert numpy.all(mtie_test.isWithinTolerance(fast_mtie))

    def test_multiprocess_mtie(self, setup: TestConfiguration):
        number_observations = 15

        t1 = time.clock()
        direct_mtie, direct_observation_intervals = calculate_direct_mtie(
            number_observations, setup
        )

        t = time.clock() - t1

        # Test the multiprocessing MTIE calculation
        ft1 = time.clock()
        fast_mtie, fast_observation_intervals = fast_mtie_calculation(
            setup.local_time_seconds,
            setup.reference_time_seconds,
            setup.time_step_seconds,
            number_observations,
            maximum_number_workers=4,
        )
        ft = time.clock() - ft1

        assert numpy.all(numpy.less(ft, t))
        assert len(direct_mtie) == len(fast_mtie)
        assert len(direct_observation_intervals) == len(fast_observation_intervals)

        assert numpy.all(direct_observation_intervals == fast_observation_intervals)

        mtie_test = ToleranceValue(direct_mtie, 0.1, ToleranceUnit["percent"])
        assert numpy.all(mtie_test.isWithinTolerance(fast_mtie))
