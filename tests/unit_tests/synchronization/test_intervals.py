#
# Copyright 2017 Russell Smiley
#
# This file is part of timetools.
#
# timetools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# timetools is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with timetools.  If not, see <http://www.gnu.org/licenses/>.
#

import numpy

from timetools.signal_processing.tolerance import ToleranceUnit, ToleranceValue
from timetools.synchronization.intervals import (
    generate_log_interval_scale,
    generate_monotonic_log_scale,
)


class TestIntervals:
    def test_calculate_log_interval_scale1(self):
        min_value = 10
        max_value = 120
        number_points = 11

        result = generate_log_interval_scale(min_value, max_value, number_points)

        assert len(result) == number_points

        min_value_tolerance = ToleranceValue(min_value, 0.1, ToleranceUnit["percent"])
        max_value_tolerance = ToleranceValue(max_value, 0.1, ToleranceUnit["percent"])
        assert min_value_tolerance.isWithinTolerance(
            result[0]
        ) and max_value_tolerance.isWithinTolerance(result[-1])

        # A logarithmic sequence will be evenly spaced in the logarithmic domain
        log_intervals = numpy.diff(numpy.log10(result))

        interval_tolerance = ToleranceValue(
            numpy.mean(log_intervals), 0.1, ToleranceUnit["percent"]
        )
        assert numpy.all(interval_tolerance.isWithinTolerance(log_intervals))

    def test_generate_monotonic_log_scale1(self):
        min_value = 10
        max_value = 25
        number_points = 12
        expected_sequence = numpy.array(
            [10, 11, 12, 13, 14, 15, 16, 17, 19, 21, 23, 25]
        )

        this_array = generate_log_interval_scale(min_value, max_value, number_points)
        this_integer_array = numpy.floor(this_array)

        monotonic_intervals = generate_monotonic_log_scale(this_integer_array)

        assert isinstance(monotonic_intervals[0], numpy.intp)
        assert len(monotonic_intervals) == len(expected_sequence)
        assert numpy.all(monotonic_intervals == expected_sequence)
