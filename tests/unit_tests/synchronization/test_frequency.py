#
# Copyright 2017 Russell Smiley
#
# This file is part of timetools.
#
# timetools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# timetools is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with timetools.  If not, see <http://www.gnu.org/licenses/>.
#

import numpy

from timetools.signal_processing.tolerance import ToleranceUnit, ToleranceValue
from timetools.synchronization.frequency import (
    convert_ffo_ppb_to_hz,
    convert_ffo_ppb_to_offset_hz,
    convert_ffo_ppb_to_skew,
    convert_skew_to_ffo_ppb,
    convert_skew_to_hz,
    convert_skew_to_offset_hz,
)


class TestFrequency:
    def test_convert_skew_to_ffo_ppb_array(self):
        input_skew = numpy.array([1.003, 1.000006, 1.00000005])
        expected_ffo_ppb = numpy.array([3e6, 6e3, 50])

        actual_ffo_ppb = convert_skew_to_ffo_ppb(input_skew)

        this_tolerance = ToleranceValue(expected_ffo_ppb, 0.1, ToleranceUnit["percent"])
        assert numpy.all(this_tolerance.isWithinTolerance(actual_ffo_ppb))

    def test_convert_skew_to_ffo_ppb_float(self):
        input_skew = 1.003
        expected_ffo_ppb = 3e6

        actual_ffo_ppb = convert_skew_to_ffo_ppb(input_skew)

        this_tolerance = ToleranceValue(expected_ffo_ppb, 0.1, ToleranceUnit["percent"])
        assert numpy.all(this_tolerance.isWithinTolerance(actual_ffo_ppb))

    def test_convert_ffo_ppb_to_skew_array(self):
        input_ffo_ppb = numpy.array([3e6, 6e3, 50])
        expected_skew = numpy.array([1.003, 1.000006, 1.00000005])

        actual_skew = convert_ffo_ppb_to_skew(input_ffo_ppb)

        this_tolerance = ToleranceValue(
            (expected_skew - 1), 0.1, ToleranceUnit["percent"]
        )
        assert numpy.all(this_tolerance.isWithinTolerance(actual_skew - 1))

    def test_convert_ffo_ppb_to_skew_float(self):
        input_ffo_ppb = 3e6
        expected_skew = 1.003

        actual_skew = convert_ffo_ppb_to_skew(input_ffo_ppb)

        this_tolerance = ToleranceValue(
            (expected_skew - 1), 0.1, ToleranceUnit["percent"]
        )
        assert numpy.all(this_tolerance.isWithinTolerance(actual_skew - 1))

    def test_convert_ffo_ppb_to_offset_hz1_array(self):
        reference_frequency_hz = 10e6
        input_ffo_ppb = numpy.array([300, 6000, 50])
        expected_offset_hz = numpy.array([3, 60, 0.5])

        actual_offset_hz = convert_ffo_ppb_to_offset_hz(
            input_ffo_ppb, reference_frequency_hz
        )

        this_tolerance = ToleranceValue(
            expected_offset_hz, 0.1, ToleranceUnit["percent"]
        )
        assert numpy.all(this_tolerance.isWithinTolerance(actual_offset_hz))

    def test_convert_ffo_ppb_to_offset_hz1_array_ref(self):
        reference_frequency_hz = numpy.array([10e6, 9e6, 11e6])
        input_ffo_ppb = numpy.array([300, 6000, 50])
        expected_offset_hz = numpy.array([3, 54, 0.55])

        actual_offset_hz = convert_ffo_ppb_to_offset_hz(
            input_ffo_ppb, reference_frequency_hz
        )

        this_tolerance = ToleranceValue(
            expected_offset_hz, 0.1, ToleranceUnit["percent"]
        )
        assert numpy.all(this_tolerance.isWithinTolerance(actual_offset_hz))

    def test_convert_ffo_ppb_to_offset_hz1_float(self):
        reference_frequency_hz = 10e6
        input_ffo_ppb = 300
        expected_offset_hz = 3

        actual_offset_hz = convert_ffo_ppb_to_offset_hz(
            input_ffo_ppb, reference_frequency_hz
        )

        this_tolerance = ToleranceValue(
            expected_offset_hz, 0.1, ToleranceUnit["percent"]
        )
        assert numpy.all(this_tolerance.isWithinTolerance(actual_offset_hz))

    def test_convert_ffo_ppb_to_offset_hz2(self):
        reference_frequency_hz = numpy.array([10e6, 1.5e9, 200e6])
        input_offset_ppb = numpy.array([300, 6000, 50])
        expected_offset_hz = numpy.array([3, 9000, 10])

        actual_offset_hz = convert_ffo_ppb_to_offset_hz(
            input_offset_ppb, reference_frequency_hz
        )

        this_tolerance = ToleranceValue(
            expected_offset_hz, 0.1, ToleranceUnit["percent"]
        )
        assert numpy.all(this_tolerance.isWithinTolerance(actual_offset_hz))

    def test_convert_skew_to_offset_hz1_array(self):
        reference_frequency_hz = 10e6
        input_ffo_ppb = numpy.array([300, 6000, 50])
        input_skew = convert_ffo_ppb_to_skew(input_ffo_ppb)
        expected_offset_hz = numpy.array([3, 60, 0.5])

        actual_offset_hz = convert_skew_to_offset_hz(input_skew, reference_frequency_hz)

        this_tolerance = ToleranceValue(
            expected_offset_hz, 0.1, ToleranceUnit["percent"]
        )
        assert numpy.all(this_tolerance.isWithinTolerance(actual_offset_hz))

    def test_convert_skew_to_offset_hz1_array_ref(self):
        reference_frequency_hz = numpy.array([10e6, 9e6, 11e6])
        input_ffo_ppb = numpy.array([300, 6000, 50])
        input_skew = convert_ffo_ppb_to_skew(input_ffo_ppb)
        expected_offset_hz = numpy.array([3, 54, 0.55])

        actual_offset_hz = convert_skew_to_offset_hz(input_skew, reference_frequency_hz)

        this_tolerance = ToleranceValue(
            expected_offset_hz, 0.1, ToleranceUnit["percent"]
        )
        assert numpy.all(this_tolerance.isWithinTolerance(actual_offset_hz))

    def test_convert_skew_to_offset_hz1_float(self):
        reference_frequency_hz = 10e6
        input_ffo_ppb = 300
        input_skew = convert_ffo_ppb_to_skew(input_ffo_ppb)
        expected_offset_hz = 3

        actual_offset_hz = convert_skew_to_offset_hz(input_skew, reference_frequency_hz)

        this_tolerance = ToleranceValue(
            expected_offset_hz, 0.1, ToleranceUnit["percent"]
        )
        assert numpy.all(this_tolerance.isWithinTolerance(actual_offset_hz))

    def test_convert_skew_to_offset_hz2(self):
        reference_frequency_hz = numpy.array([10e6, 1.5e9, 200e6])
        input_ffo_ppb = numpy.array([300, 6000, 50])
        input_skew = convert_ffo_ppb_to_skew(input_ffo_ppb)
        expected_offset_hz = numpy.array([3, 9000, 10])

        actual_offset_hz = convert_skew_to_offset_hz(input_skew, reference_frequency_hz)

        this_tolerance = ToleranceValue(
            expected_offset_hz, 0.1, ToleranceUnit["percent"]
        )
        assert numpy.all(this_tolerance.isWithinTolerance(actual_offset_hz))

    def test_convert_ffo_ppb_to_hz1_array(self):
        reference_frequency_hz = 10e6
        input_ffo_ppb = numpy.array([300, 6000, 50])
        expected_frequency_hz = numpy.array([3, 60, 0.5]) + reference_frequency_hz

        actual_frequency_hz = convert_ffo_ppb_to_hz(
            input_ffo_ppb, reference_frequency_hz
        )

        this_tolerance = ToleranceValue(
            expected_frequency_hz, 0.1, ToleranceUnit["percent"]
        )
        assert numpy.all(this_tolerance.isWithinTolerance(actual_frequency_hz))

    def test_convert_ffo_ppb_to_hz1_array_ref(self):
        reference_frequency_hz = numpy.array([10e6, 9e6, 11e6])
        input_ffo_ppb = numpy.array([300, 6000, 50])
        expected_frequency_hz = numpy.array([3, 54, 0.55]) + reference_frequency_hz

        actual_frequency_hz = convert_ffo_ppb_to_hz(
            input_ffo_ppb, reference_frequency_hz
        )

        this_tolerance = ToleranceValue(
            expected_frequency_hz, 0.1, ToleranceUnit["percent"]
        )
        assert numpy.all(this_tolerance.isWithinTolerance(actual_frequency_hz))

    def test_convert_ffo_ppb_to_hz1_float(self):
        reference_frequency_hz = 10e6
        input_ffo_ppb = 300
        expected_frequency_hz = 3 + reference_frequency_hz

        actual_frequency_hz = convert_ffo_ppb_to_hz(
            input_ffo_ppb, reference_frequency_hz
        )

        this_tolerance = ToleranceValue(
            expected_frequency_hz, 0.1, ToleranceUnit["percent"]
        )
        assert numpy.all(this_tolerance.isWithinTolerance(actual_frequency_hz))

    def test_convert_ffo_ppb_to_hz2(self):
        reference_frequency_hz = numpy.array([10e6, 1.5e9, 200e6])
        input_ffo_ppb = numpy.array([300, 6000, 50])
        expected_frequency_hz = numpy.array([3, 9000, 10]) + reference_frequency_hz

        actual_frequency_hz = convert_ffo_ppb_to_hz(
            input_ffo_ppb, reference_frequency_hz
        )

        this_tolerance = ToleranceValue(
            expected_frequency_hz, 0.1, ToleranceUnit["percent"]
        )
        assert numpy.all(this_tolerance.isWithinTolerance(actual_frequency_hz))

    def test_convert_skew_to_hz1_array(self):
        reference_frequency_hz = 10e6
        input_ffo_ppb = numpy.array([300, 6000, 50])
        input_skew = convert_ffo_ppb_to_skew(input_ffo_ppb)
        expected_frequency_hz = numpy.array([3, 60, 0.5]) + reference_frequency_hz

        actual_frequency_hz = convert_skew_to_hz(input_skew, reference_frequency_hz)

        this_tolerance = ToleranceValue(
            expected_frequency_hz - reference_frequency_hz,
            0.1,
            ToleranceUnit["percent"],
        )
        assert numpy.all(
            this_tolerance.isWithinTolerance(
                actual_frequency_hz - reference_frequency_hz
            )
        )

    def test_convert_skew_to_hz1_array_ref(self):
        reference_frequency_hz = numpy.array([10e6, 9e6, 11e6])
        input_ffo_ppb = numpy.array([300, 6000, 50])
        input_skew = convert_ffo_ppb_to_skew(input_ffo_ppb)
        expected_frequency_hz = numpy.array([3, 54, 0.55]) + reference_frequency_hz

        actual_frequency_hz = convert_skew_to_hz(input_skew, reference_frequency_hz)

        this_tolerance = ToleranceValue(
            expected_frequency_hz - reference_frequency_hz,
            0.1,
            ToleranceUnit["percent"],
        )
        assert numpy.all(
            this_tolerance.isWithinTolerance(
                actual_frequency_hz - reference_frequency_hz
            )
        )

    def test_convert_skew_to_hz1_float(self):
        reference_frequency_hz = 10e6
        input_ffo_ppb = 300
        input_skew = convert_ffo_ppb_to_skew(input_ffo_ppb)
        expected_frequency_hz = 3 + reference_frequency_hz

        actual_frequency_hz = convert_skew_to_hz(input_skew, reference_frequency_hz)

        this_tolerance = ToleranceValue(
            expected_frequency_hz - reference_frequency_hz,
            0.1,
            ToleranceUnit["percent"],
        )
        assert numpy.all(
            this_tolerance.isWithinTolerance(
                actual_frequency_hz - reference_frequency_hz
            )
        )

    def test_convert_skew_to_hz2(self):
        reference_frequency_hz = numpy.array([10e6, 1.5e9, 200e6])
        input_ffo_ppb = numpy.array([300, 6000, 50])
        input_skew = convert_ffo_ppb_to_skew(input_ffo_ppb)
        expected_frequency_hz = numpy.array([3, 9000, 10]) + reference_frequency_hz

        actual_frequency_hz = convert_skew_to_hz(input_skew, reference_frequency_hz)

        this_tolerance = ToleranceValue(
            expected_frequency_hz - reference_frequency_hz,
            0.1,
            ToleranceUnit["percent"],
        )
        assert numpy.all(
            this_tolerance.isWithinTolerance(
                actual_frequency_hz - reference_frequency_hz
            )
        )
