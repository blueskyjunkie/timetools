#
# Copyright 2017 Russell Smiley
#
# This file is part of timetools.
#
# timetools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# timetools is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with timetools.  If not, see <http://www.gnu.org/licenses/>.
#

import numpy

from timetools.signal_processing.tolerance import ToleranceUnit, ToleranceValue
from timetools.synchronization.analysis.itut_g810 import calculate_time_error
from timetools.synchronization.clock import ClockModel, calculate_local_time_from_ffo
from timetools.synchronization.oscillator import OscillatorModel
from timetools.synchronization.oscillator.noise.gaussian import GaussianNoise
from timetools.synchronization.time import ReferenceGenerator


class TestClock:
    def test_calculate_local_time_from_ffo1(self):
        reference_time_seconds = numpy.array([1, 2, 3])
        ffo_ppb = numpy.array([3, -6, 9]) * 1e8

        expected_local_time_seconds = numpy.array([1, 2.3, 2.7])
        actual_local_time_seconds = calculate_local_time_from_ffo(
            reference_time_seconds, ffo_ppb
        )

        this_tolerance = ToleranceValue(
            expected_local_time_seconds, 1e-6, ToleranceUnit["relative"]
        )
        assert this_tolerance.isWithinTolerance(actual_local_time_seconds)

    def test_calculate_local_time_from_ffo2(self):
        initial_conditions = [numpy.array([0, 0, 0]), numpy.array([9 * 1e8, 3, 2.7])]
        reference_time_seconds = [numpy.array([1, 2, 3]), numpy.array([4, 5, 6, 7])]
        ffo_ppb = [numpy.array([3, -6, 9]) * 1e8, numpy.array([1, -2, 4, 0]) * 1e8]

        expected_local_time_seconds = [
            numpy.array([1, 2.3, 2.7]),
            numpy.array([4.6, 5.7, 6.5, 7.9]),
        ]

        concatenated_reference_time_seconds = numpy.concatenate(
            (reference_time_seconds[0], reference_time_seconds[1])
        )
        concatenated_ffo_ppb = numpy.concatenate((ffo_ppb[0], ffo_ppb[1]))
        concatenated_expected_local_time_seconds = numpy.concatenate(
            (expected_local_time_seconds[0], expected_local_time_seconds[1])
        )
        actual_concatenated_local_time_seconds = calculate_local_time_from_ffo(
            concatenated_reference_time_seconds,
            concatenated_ffo_ppb,
            initial_conditions[0][0],
            initial_conditions[0][1],
            initial_conditions[0][2],
        )

        concatenated_tolerance = ToleranceValue(
            concatenated_expected_local_time_seconds, 1e-6, ToleranceUnit["relative"]
        )
        assert concatenated_tolerance.isWithinTolerance(
            actual_concatenated_local_time_seconds
        )

        for i in range(2):
            actual_local_time_seconds = calculate_local_time_from_ffo(
                reference_time_seconds[i],
                ffo_ppb[i],
                initial_conditions[i][0],
                initial_conditions[i][1],
                initial_conditions[i][2],
            )

            this_tolerance = ToleranceValue(
                expected_local_time_seconds[i], 1e-6, ToleranceUnit["relative"]
            )
            assert this_tolerance.isWithinTolerance(actual_local_time_seconds)

    def test_clock1(self):
        time_step_seconds = 1 / 16
        number_samples = 10

        reference_time_generator = ReferenceGenerator(time_step_seconds)
        reference_time_seconds = reference_time_generator.generate(number_samples)

        clock_model = ClockModel(OscillatorModel())

        local_time_seconds, instantaneous_lo_ffo_ppb = clock_model.generate(
            reference_time_seconds
        )

        assert numpy.all(local_time_seconds == reference_time_seconds)
        assert numpy.all(instantaneous_lo_ffo_ppb == 0)

    def test_clock2(self):
        time_step_seconds = 1 / 16
        number_samples = 10

        reference_time_generator = ReferenceGenerator(time_step_seconds)
        reference_time_seconds = reference_time_generator.generate(number_samples)

        clock_model = ClockModel(OscillatorModel())

        local_time_seconds = numpy.array([])
        instantaneous_lo_ffo_ppb = numpy.array([])
        for this_time in reference_time_seconds:
            (
                this_local_time_seconds,
                this_instantaneous_lo_ffo_ppb,
            ) = clock_model.generate(numpy.array([this_time]))
            local_time_seconds = numpy.concatenate(
                (local_time_seconds, this_local_time_seconds)
            )
            instantaneous_lo_ffo_ppb = numpy.concatenate(
                (instantaneous_lo_ffo_ppb, this_instantaneous_lo_ffo_ppb)
            )

        assert numpy.all(local_time_seconds == reference_time_seconds)
        assert numpy.all(instantaneous_lo_ffo_ppb == 0)

    def test_clock3(self):
        time_step_seconds = 1 / 16
        number_samples = 10

        initial_ffo_ppb = 100e3

        expected_local_time_seconds = numpy.array(
            [
                0.0,
                0.06250625,
                0.1250125,
                0.18751875,
                0.250025,
                0.31253125,
                0.3750375,
                0.43754375,
                0.50005,
                0.56255625,
            ]
        )
        reference_time_generator = ReferenceGenerator(time_step_seconds)
        reference_time_seconds = reference_time_generator.generate(number_samples)

        expected_time_error_seconds = calculate_time_error(
            expected_local_time_seconds, reference_time_seconds
        )

        clock_model = ClockModel(OscillatorModel(initial_ffo_ppb=initial_ffo_ppb))

        actual_local_time_seconds, instantaneous_lo_ffo_ppb = clock_model.generate(
            reference_time_seconds
        )
        actual_time_error_seconds = calculate_time_error(
            actual_local_time_seconds, reference_time_seconds
        )

        assert numpy.all(instantaneous_lo_ffo_ppb == initial_ffo_ppb)
        this_tolerance = ToleranceValue(
            expected_time_error_seconds, 1e-6, ToleranceUnit["relative"]
        )
        assert numpy.all(this_tolerance.isWithinTolerance(actual_time_error_seconds))

    def test_clock4(self):
        time_step_seconds = 1 / 16
        number_samples = 10

        initial_ffo_ppb = 100e3

        expected_local_time_seconds = numpy.array(
            [
                0.0,
                0.06250625,
                0.1250125,
                0.18751875,
                0.250025,
                0.31253125,
                0.3750375,
                0.43754375,
                0.50005,
                0.56255625,
            ]
        )
        reference_time_generator = ReferenceGenerator(time_step_seconds)
        reference_time_seconds = reference_time_generator.generate(number_samples)

        expected_time_error_seconds = calculate_time_error(
            expected_local_time_seconds, reference_time_seconds
        )

        clock_model = ClockModel(OscillatorModel(initial_ffo_ppb=initial_ffo_ppb))

        actual_local_time_seconds = numpy.array([])
        instantaneous_lo_ffo_ppb = numpy.array([])
        for i in range(0, 2):
            this_iteration_reference_time_seconds = reference_time_seconds[:5]
            if i == 1:
                this_iteration_reference_time_seconds = reference_time_seconds[5:]

            (
                this_iteration_actual_local_time_seconds,
                this_iteration_instantaneous_lo_ffo_ppb,
            ) = clock_model.generate(this_iteration_reference_time_seconds)

            actual_local_time_seconds = numpy.concatenate(
                (actual_local_time_seconds, this_iteration_actual_local_time_seconds)
            )
            instantaneous_lo_ffo_ppb = numpy.concatenate(
                (instantaneous_lo_ffo_ppb, this_iteration_instantaneous_lo_ffo_ppb)
            )

        actual_time_error_seconds = calculate_time_error(
            actual_local_time_seconds, reference_time_seconds
        )

        assert numpy.all(instantaneous_lo_ffo_ppb == initial_ffo_ppb)
        this_tolerance = ToleranceValue(
            expected_time_error_seconds, 1e-6, ToleranceUnit["relative"]
        )
        assert numpy.all(this_tolerance.isWithinTolerance(actual_time_error_seconds))

    def test_clock_random_seed(self):
        time_step_seconds = 1 / 16
        number_samples = 10

        initial_ffo_ppb = 100e3

        reference_time_generator = ReferenceGenerator(time_step_seconds)
        reference_time_seconds = reference_time_generator.generate(number_samples)

        # Two models with the same seed should generate the exact same sequence
        clock_model1 = ClockModel(
            OscillatorModel(
                initial_ffo_ppb=initial_ffo_ppb, noise_model=GaussianNoise(seed=1459)
            )
        )
        clock_model2 = ClockModel(
            OscillatorModel(
                initial_ffo_ppb=initial_ffo_ppb, noise_model=GaussianNoise(seed=1459)
            )
        )
        clock_model3 = ClockModel(
            OscillatorModel(
                initial_ffo_ppb=initial_ffo_ppb, noise_model=GaussianNoise(seed=5986)
            )
        )

        actual_local_time_seconds1, instantaneous_lo_ffo_ppb1 = clock_model1.generate(
            reference_time_seconds
        )
        actual_local_time_seconds2, instantaneous_lo_ffo_ppb2 = clock_model2.generate(
            reference_time_seconds
        )
        actual_local_time_seconds3, instantaneous_lo_ffo_ppb3 = clock_model3.generate(
            reference_time_seconds
        )

        assert numpy.all(instantaneous_lo_ffo_ppb1 == instantaneous_lo_ffo_ppb2)
        assert not numpy.all(instantaneous_lo_ffo_ppb1 == instantaneous_lo_ffo_ppb3)
        assert numpy.all(actual_local_time_seconds1 == actual_local_time_seconds2)
        assert not numpy.all(actual_local_time_seconds1 == actual_local_time_seconds3)
